export type AmplifyDependentResourcesAttributes = {
    auth: {
        profesnextapp38d43a72: {
            IdentityPoolId: 'string'
            IdentityPoolName: 'string'
            HostedUIDomain: 'string'
            OAuthMetadata: 'string'
            UserPoolId: 'string'
            UserPoolArn: 'string'
            UserPoolName: 'string'
            AppClientIDWeb: 'string'
            AppClientID: 'string'
        }
    }
}
