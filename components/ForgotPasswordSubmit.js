import Input from './Input'

function FogotPasswordSubmit({ setUiState, onChange, forgotPasswordSubmit }) {
    return (
        <>
            <p className="text-3xl font-black">Recuperar contraseña</p>
            <div className="mt-10">
                <label className="text-sm">Código de confirmación</label>
                <Input onChange={onChange} name="authCode" />
            </div>
            <div className="mt-6">
                <label className="text-sm">Nueva contraseña</label>
                <Input type="password" name="password" onChange={onChange} />
            </div>
            <button
                onClick={() => forgotPasswordSubmit()}
                className="text-white w-full mt-4 bg-[//#endregionF900BF] p-3 rounded"
            >
                Continuar
            </button>
            <button
                onClick={() => setUiState('signIn')}
                className="text-sm mt-6 text-pink-500"
            >
                Cancelar
            </button>
        </>
    )
}

export default FogotPasswordSubmit
