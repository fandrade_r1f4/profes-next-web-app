import Input from './Input'

function FogotPassword({ setUiState, onChange, forgotPassword }) {
    return (
        <>
            <p className="text-3xl font-black">Recuperar contraseña</p>
            <div className="mt-10">
                <label className="text-sm">Email</label>
                <Input onChange={onChange} name="email" />
            </div>
            <button
                onClick={() => forgotPassword()}
                className="text-white w-full mt-4 bg-[#F900BF] p-3 rounded"
            >
                Continuar
            </button>
            <button
                onClick={() => setUiState('signIn')}
                className="text-sm mt-6 text-pink-500"
            >
                Cancelar
            </button>
        </>
    )
}

export default FogotPassword
