import React, { useState, useEffect } from 'react'
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import ClipLoader from 'react-spinners/ClipLoader'
import Select from 'react-select'
import Toggle from 'react-toggle'
import axios from 'axios'


const override = {
    position: 'fixed',
    left: '50%',
    top: '50%',
}

const Column = ({column, handleColumnsOptions, handleColumnsTitles, errorTitleColumnA, errorTitleColumnB, errorsIdsColumnA, errorsIdsColumnB, handleTotalOptionsColumns, handleRemoveIdFromError, cleanColumnsForm}) => {
    const numberOptionsColum = [
        { value: 2, label: '2' },
        { value: 3, label: '3' },
        { value: 4, label: '4' },
        { value: 5, label: '5' },
    ]
    const idType = [
        { value: 'number', label: 'Número', isDisabled: column === 'A' ? false : true },
        { value: 'letter', label: 'Letra', isDisabled: column === 'B' ? false : true  },
    ]

    const [selectedNumberOptionsColumn, setSelectedNumberOptionsColumn] =
        useState(numberOptionsColum[1])
    const [optionsColumn, setOptionsColumn] = useState([])
    

    const [columnTitle, setColumnTitle] = useState('')

    const [hasImagesColumn, setHasImagesColumn] = useState(false)

    const [isLoading, setIsLoading] = useState(false)




    const BUCKET_URL = 'https://profe-santi-questions.s3.amazonaws.com/'




    const getOptions = (clean) => {
        let letters = ['A', 'B', 'C', 'D', 'E']
        let numbers = [1, 2, 3, 4, 5]

        let arrOptions = []
        for (let i = 0; i < selectedNumberOptionsColumn.value; i++) {
            if (column === 'A') {
                if(clean){
                    arrOptions.push({
                        id: numbers[i],
                        option: ''
                    })
                } else {
                    arrOptions.push({
                        id: numbers[i],
                        option: optionsColumn.length > 0 && optionsColumn[i] && optionsColumn[i].value || ''
                    })
                }
             
            } else {
                if(clean){
                    arrOptions.push({
                        id: letters[i],
                        option: ''
                    })
                } else {
                    arrOptions.push({
                        id: letters[i],
                        optiom: optionsColumn.length > 0 && optionsColumn[i] && optionsColumn[i].value || ''
                    })
                }
              
            }
        }

        return arrOptions
    }

    useEffect(() => {
        let options = getOptions(false)
        setOptionsColumn(options)
        handleTotalOptionsColumns(column, options.length)
        handleColumnsOptions(column, options, options.length)

    }, [ selectedNumberOptionsColumn])

    useEffect(() => {
        let options = getOptions(true);
        setOptionsColumn(options)
        handleColumnsOptions(column, options, options.length)

    }, [hasImagesColumn])

    useEffect(() => {
        let options = getOptions(true);
        setOptionsColumn(options)
        handleColumnsOptions(column, options, options.length)
        setColumnTitle('');
    }, [cleanColumnsForm])

    const handleSetOption = (id, valueOption) => {
        let newOptions = []

        newOptions = optionsColumn.map((option) => {
            if (option.id === id) {
                return {
                    ...option,
                    option: valueOption,
                }
            }
            return option
        })
        setOptionsColumn(newOptions)
        handleColumnsOptions(column, newOptions, newOptions.length)
        handleRemoveIdFromError(column, id)
    }

    const handleSetTitle = (title) => {
        setColumnTitle(title);
        handleColumnsTitles(column, title)
    }



   const selectOptionFile = (e, id) => {
    uploadOptionFile(e.target.files[0], id)
   }

   const isValidImage = (size, format) => {
    if(size > 16000000 || format !== 'image/png') return false
    return true;
}

   const uploadOptionFile = async (fileText, id) => {
    if(!isValidImage(fileText.size, fileText.type)){
        toast.error(
            `Hubo un problema cargando la imagen. Favor de asegurarte que pese máximo 2MB y sea formato .png 🤔`
        )
    } else{
        try {
            setIsLoading(true);
            let { data } = await axios.post('/api/s3/uploadFile', {
                name: fileText.name,
                type: fileText.type,
            })
            const url = data.url
            await axios.put(url, fileText, {
                headers: {
                    'Content-type': fileText.type,
                    'Access-Control-Allow-Origin': '*',
                },
            })
            toast.success(`Imagen cargada correctamente 🙂`)
            setIsLoading(false)
            let newOptions = []
    
            newOptions = optionsColumn.map((option) => {
                if (option.id === id) {
                    return {
                        ...option,
                        option:  (BUCKET_URL + fileText.name).replace(/\s+/g, ''),
                    }
                }
                return option
            })
            setOptionsColumn(newOptions)
            handleColumnsOptions(column, newOptions, newOptions.length)
            handleRemoveIdFromError(column, id)
    
            } catch (e) {
            toast.error(
                `Hubo un problema cargando la imagen. Favor de intentar más tarde 🤔`
            )
            setIsLoading(false)
            console.log(e)
        }
    }
   
}



    return (
        <div className="flex flex-col mt-4 w-1/2">
            {isLoading && (
                <ClipLoader
                    color={'#ec4899'}
                    size={40}
                    aria-label="Loading Spinner"
                    data-testid="loader"
                    loading={isLoading}
                    cssOverride={override}
                />
            )}

            <div className="mb-6">
                <label htmlFor="default-input" className="block mb-2">
                    Título Columna
                </label>
                <input
                    type="text"
                    id="default-input"
                    className="text-gray-900 w-72 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5"
                    onChange={(e) => handleSetTitle(e.target.value)}
                    value={columnTitle}
                />
                {errorTitleColumnA && <span className='text-rose-600'>{errorTitleColumnA}</span>}
                {errorTitleColumnB && <span className='text-rose-600'>{errorTitleColumnB}</span>}

            </div>

            <div className="mt-8 flex items-center">
                <Toggle
                    id="text-image-status"
                    onChange={() => {
                    setHasImagesColumn(!hasImagesColumn)
                    }}
                    checked={hasImagesColumn}
                />
                <label
                    className="ml-2"
                    htmlFor="text-image-status"
                >
                    Tiene imagenes la columna?
                </label>
             </div>

            <div className="mt-8 flex">
                <div className="flex flex-col">
                    <span className="w-54">Total Opciones</span>
                    <Select
                        defaultValue={numberOptionsColum[1]}
                        onChange={setSelectedNumberOptionsColumn}
                        options={numberOptionsColum}
                        className="w-30"
                    />
                </div>
                <div className="flex flex-col ml-6">
                    <span className="w-54">Identificador</span>
                    <Select
                        options={idType}
                        className="w-30"
                        value={column === 'A' ? idType[0]: idType[1]}
                    />
                </div>
            </div>
            <div className="mt-8">
                {optionsColumn.map((option, index) => (
                    <div key={index}>
                    <div className="mb-2 mt-2">
                        <label
                            htmlFor="large-input"
                            className="block mb-2 text-sm"
                        >
                            {option.id}.{' '}
                        </label>
                        {hasImagesColumn ?  
                        <>
                            <input
                                className="mt-2"
                                type="file"
                                onChange={(e) =>
                                    selectOptionFile(e, option.id)
                                }
                            />

                            {option.option && (
                                <img
                                    className="mt-4"
                                    src={option.option}
                                />
                            )}
                        </> :
                        <input
                            type="text"
                            id="large-input"
                            className="block w-72 p-4"
                            value={option.option}
                            onChange={(e) =>
                                handleSetOption(option.id, e.target.value)
                            }
                        />
                        }
                        

                    </div>
                    <div>
                    {errorsIdsColumnA && errorsIdsColumnA.includes(option.id) && <span className='text-rose-600'>Tienes que llenar esta opción</span>}
                        {errorsIdsColumnB && errorsIdsColumnB.includes(option.id) && <span className='text-rose-600'>Tienes que llenar esta opción</span>} 

                    </div>
                    </div>
                ))}
            </div>
        </div>
    )
}

export default Column
