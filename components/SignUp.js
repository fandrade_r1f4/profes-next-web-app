import Input from './Input'

function SignUp({ setUiState, signUp, onChange }) {
    return (
        <div className="container max-w-lg">
            <p className="text-3xl font-black">Regístrate</p>
            <div className="mt-10">
                <label className="text-sm">Email</label>
                <Input onChange={onChange} name="email" />
            </div>
            <div className="mt-7">
                <p className="text-sm">Contraseña</p>
                <Input name="password" onChange={onChange} type="password" />
            </div>
            <button
                onClick={signUp}
                className="text-white w-full mt-6 bg-[#F900BF] p-3 rounded"
            >
                Continuar
            </button>
            <p className="mt-12 text-sm font-light">
                Ya tienes una cuenta?
                <span
                    className="cursor-pointer text-pink-600"
                    onClick={() => setUiState('signIn')}
                >
                    {' '}
                    Iniciar sesión.
                </span>
            </p>
        </div>
    )
}

export default SignUp
