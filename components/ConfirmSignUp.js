import Input from './Input'

function ConfirmSignUp({ setUiState, onChange, confirmSignUp }) {
    return (
        <>
            <p className="text-3xl font-black">Confirmar Registro</p>
            <div className="mt-10">
                <label className="text-sm">Código de confirmación</label>
                <Input onChange={onChange} name="authCode" />
            </div>
            <button
                onClick={() => confirmSignUp()}
                className="text-white w-full mt-4 bg-[#F900BF] p-3 rounded"
            >
                Continuar
            </button>
            <button
                onClick={() => setUiState('signIn')}
                className="text-sm mt-6 text-pink-500"
            > Cancelar
            
            </button>
        </>
    )
}

export default ConfirmSignUp
