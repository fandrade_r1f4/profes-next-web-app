import Input from './Input'
//import SocialSignIn from './SocialSignIn'

function SignIn({ setUiState, onChange, signIn }) {
    return (
        <div className="container max-w-lg">
            <p className="text-3xl font-black">Iniciar sesión</p>
            <div className="mt-10">
                <label className="text-sm">Email</label>
                <Input onChange={onChange} name="email" />
            </div>
            <div className="mt-7">
                <label className="text-sm">
                    Contraseña
                    <span
                        onClick={() => setUiState('forgotPassword')}
                        className="ml-8 sm:ml-48 text-pink-500"
                    >
                        Olvidaste tu contraseña?
                    </span>
                </label>
                <Input type="password" name="password" onChange={onChange} />
            </div>
            <button
                onClick={signIn}
                className="text-white w-full mt-6 bg-[#F900BF] p-3 rounded"
            >
                Continuar
            </button>
            {/*<SocialSignIn />*/}
            <p className="mt-12 text-sm font-light">
                No tienes una cuenta?
                <span
                    onClick={() => setUiState('signUp')}
                    role="button"
                    className="cursor-pointer text-pink-600"
                >
                    {' '}
                    Regístrate.
                </span>
            </p>
        </div>
    )
}

export default SignIn
