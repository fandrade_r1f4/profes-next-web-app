import { useState, useEffect, useMemo } from 'react'
import { Auth } from 'aws-amplify'
import axios from 'axios'
import ClipLoader from 'react-spinners/ClipLoader'
import { useRouter } from 'next/router'
import { useTable, useFilters, usePagination } from 'react-table'
import matchSorter from 'match-sorter'

function DefaultColumnFilter({
    column: { filterValue, preFilteredRows, setFilter },
}) {
    const count = preFilteredRows.length

    return (
        <input
            value={filterValue || ''}
            onChange={(e) => {
                setFilter(e.target.value || undefined)
            }}
            placeholder={`Buscar...`}
        />
    )
}

function SelectColumnFilter({
    column: { filterValue, setFilter, preFilteredRows, id },
}) {
    const options = useMemo(() => {
        const options = new Set()
        preFilteredRows.forEach((row) => {
            options.add(row.values[id])
        })
        return [...options.values()]
    }, [id, preFilteredRows])

    return (
        <select
            value={filterValue}
            onChange={(e) => {
                setFilter(e.target.value || undefined)
            }}
        >
            <option value="">Todas</option>
            {options.map((option, i) => (
                <option key={i} value={option}>
                    {option}
                </option>
            ))}
        </select>
    )
}

function fuzzyTextFilterFn(rows, id, filterValue) {
    return matchSorter(rows, filterValue, { keys: [(row) => row.values[id]] })
}

fuzzyTextFilterFn.autoRemove = (val) => !val

function Table({ columns, data }) {
    const filterTypes = useMemo(
        () => ({
            fuzzyText: fuzzyTextFilterFn,

            text: (rows, id, filterValue) => {
                return rows.filter((row) => {
                    const rowValue = row.values[id]
                    return rowValue !== undefined
                        ? String(rowValue)
                              .toLowerCase()
                              .startsWith(String(filterValue).toLowerCase())
                        : true
                })
            },
        }),
        []
    )

    const defaultColumn = useMemo(
        () => ({
            Filter: DefaultColumnFilter,
        }),
        []
    )

    const {
        getTableProps,
        headerGroups,
        prepareRow,
        state,
        visibleColumns,
        page,
        canPreviousPage,
        canNextPage,
        pageOptions,
        pageCount,
        gotoPage,
        nextPage,
        previousPage,
        setPageSize,
        rows,
        state: { pageIndex, pageSize },
    } = useTable(
        {
            columns,
            data,
            defaultColumn,
            filterTypes,
            initialState: { pageIndex: 0, pageSize: 10 },
        },
        useFilters,
        usePagination
    )

    const firstPageRows = rows.slice(0, 10)

    return (
        <>
            <table {...getTableProps()} className="bg-pink-intense">
                <thead>
                    {headerGroups.map((headerGroup) => (
                        <tr {...headerGroup.getHeaderGroupProps()}>
                            {headerGroup.headers.map((column) => (
                                <th {...column.getHeaderProps()}>
                                    {column.render('Header')}
                                    <div className="mt-4">
                                        {column.canFilter
                                            ? column.render('Filter')
                                            : null}
                                    </div>
                                </th>
                            ))}
                        </tr>
                    ))}
                </thead>
                {page.map((row, i) => {
                    prepareRow(row)
                    return (
                        <tr {...row.getRowProps()}>
                            {row.cells.map((cell) => {
                                return (
                                    <td {...cell.getCellProps()}>
                                        {cell.render('Cell')}
                                    </td>
                                )
                            })}
                        </tr>
                    )
                })}
            </table>
            <br />
            <div className="flex justify-center">
                <div className="w-40 flex justify-around">
                    <button
                        className="font-extrabold text-[#F900BF]"
                        onClick={() => gotoPage(0)}
                        disabled={!canPreviousPage}
                    >
                        {'<<'}
                    </button>{' '}
                    <button
                        className="font-extrabold text-[#F900BF]"
                        onClick={() => previousPage()}
                        disabled={!canPreviousPage}
                    >
                        {'<'}
                    </button>{' '}
                    <button
                        className="font-extrabold text-[#F900BF]"
                        onClick={() => nextPage()}
                        disabled={!canNextPage}
                    >
                        {'>'}
                    </button>{' '}
                    <button
                        className="font-extrabold text-[#F900BF]"
                        onClick={() => gotoPage(pageCount - 1)}
                        disabled={!canNextPage}
                    >
                        {'>>'}
                    </button>{' '}
                </div>
            </div>
            <div className="flex justify-center mt-4 ">
                <div className="w-72 flex justify-around">
                    <div>
                        <span className="text-[#F900BF]">
                            Página{' '}
                            <strong>
                                {pageIndex + 1} de {pageOptions.length}
                            </strong>{' '}
                        </span>
                    </div>
                    <div>
                        <select
                            value={pageSize}
                            onChange={(e) => {
                                setPageSize(Number(e.target.value))
                            }}
                            className="text-[#F900BF]"
                        >
                            {[10, 20, 30, 40, 50].map((pageSize) => (
                                <option key={pageSize} value={pageSize}>
                                    Mostrar {pageSize}
                                </option>
                            ))}
                        </select>
                    </div>
                </div>
            </div>
        </>
    )
}

function filterGreaterThan(rows, id, filterValue) {
    return rows.filter((row) => {
        const rowValue = row.values[id]
        return rowValue >= filterValue
    })
}

filterGreaterThan.autoRemove = (val) => typeof val !== 'number'

const override = {
    position: 'fixed',
    left: '50%',
    top: '50%',
}

function ViewQuestions() {
    const [userJwt, setUserJwt] = useState(null)
    const [questions, setQuestions] = useState([])
    const [signatures, setSignatures] = useState([])
    const router = useRouter()

    const [isLoading, setIsLoading] = useState(false)

    const columns = useMemo(
        () => [
            {
                Header: 'Materia',
                accessor: 'signature',
                Filter: SelectColumnFilter,
                filter: 'includes',
            },
            {
                Header: 'Tipo Pregunta',
                accessor: 'type',
                Filter: SelectColumnFilter,
                filter: 'includes',
            },
            {
                Header: 'Unidad',
                accessor: 'unit',
            },
            {
                Header: 'Tema',
                accessor: 'topic',
            },
            {
                Header: 'Subtema',
                accessor: 'subtopic',
            },
            {
                Header: 'Texto',
                accessor: 'text',
            },
            {
                Header: 'Subtexto',
                accessor: 'subtext',
            },
            {
                Header: 'Opción 1',
                accessor: 'option1',
            },
            {
                Header: 'Opción 2',
                accessor: 'option2',
            },
            {
                Header: 'Opción 3',
                accessor: 'option3',
            },
            {
                Header: 'Opción 4',
                accessor: 'option4',
            },
            {
                Header: 'Respuesta Correcta',
                accessor: 'correctAnswer',
            },
            {
                Header: 'Dificultad',
                accessor: 'difficulty',
                Filter: SelectColumnFilter,
                filter: 'includes',
            },
        ],
        []
    )

    useEffect(() => {
        checkUser()
    }, [])

    async function checkUser() {
        try {
            const user = await Auth.currentAuthenticatedUser()
            setUserJwt(user.signInUserSession.idToken.jwtToken)
        } catch (e) {
            router.push('/auth')
        }
    }

    useEffect(() => {
        const getData = async () => {
            try {
                setIsLoading(true)

                let { data: signatures } = await axios.get(
                    `${process.env.NEXT_PUBLIC_AMAZON_URL}/${process.env.NEXT_PUBLIC_STAGE}/signatures`,
                    {
                        headers: {
                            'x-api-key': process.env.NEXT_PUBLIC_API_KEY,
                            Authorization: 'Bearer ' + userJwt,
                        },
                    }
                )

                const { data: questions } = await axios.get(
                    `${process.env.NEXT_PUBLIC_AMAZON_URL}/${process.env.NEXT_PUBLIC_STAGE}/questions`,
                    {
                        headers: {
                            'Content-Type': 'application/json',
                            Accept: 'application/json',
                            'x-api-key': process.env.NEXT_PUBLIC_API_KEY,
                            Authorization: 'Bearer ' + userJwt,
                        },
                    }
                )

                const questionsFormatted = questions.map((question) => ({
                    ...question,
                    option1: question.options[0].value,
                    option2: question.options[1].value,
                    option3: question.options[2].value,
                    option4: question.options[3].value,
                    signature: signatures.filter(
                        (signature) => signature.id === question.idSignature
                    )[0].name,
                }))
                setIsLoading(false)
                setQuestions(questionsFormatted)
            } catch (e) {
                setIsLoading(false);
                console.log(e)
            }
        }
        if (userJwt) getData()
    }, [userJwt])

    return (
        <>
            {isLoading ? (
                <ClipLoader
                    color={'#ec4899'}
                    size={40}
                    aria-label="Loading Spinner"
                    data-testid="loader"
                    loading={isLoading}
                    cssOverride={override}
                />
            ) : (
                questions.length > 0 && (
                    <div className="container rounded-md h-auto  mt-8 py-6 mb-8 px-16 max-w-5xl ">
                        <div className="overflow-x-auto">
                            <Table columns={columns} data={questions} />
                        </div>
                    </div>
                )
            )}
        </>
    )
}

export default ViewQuestions
