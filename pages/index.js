
import React from 'react';
import Image from 'next/image'
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import Link from 'next/link'
import 'react-tabs/style/react-tabs.css';

export default function Home() {
    return (
        <div className="container h-auto  mt-8 py-6 mb-8 px-16 max-w-3xl">
            <h1 className="text-3xl font-bold text-[#F900BF]">
                Instrucciones para subir tus preguntas
            </h1>

        <div className='mt-16'>
        <h2 className="text-xl font-bold text-[#F900BF]">1. Especificar el tipo de reactivo</h2>
        <Tabs className='mt-8'>
            <TabList>
                <Tab>Cuestionamiento directo</Tab>
                <Tab>Jerarquización u ordenamiento</Tab>
                <Tab>Completamiento</Tab>
                <Tab>Relación de columnas</Tab>
                <Tab>Elección de elementos</Tab>
                <Tab>Decisión o justificación de experto</Tab>

            </TabList>

            <TabPanel>
              <p className='mt-8'>Ejemplo</p>
            <Image
              src="https://profe-santi-questions.s3.amazonaws.com/cuestionamiento_directo-removebg-preview.png"
              alt="Preguntas directas"
              width={500}
              height={500}
              className="mt-8"
            />
            </TabPanel>
            <TabPanel>
              <p className='mt-8'>Ejemplo</p>
            <Image
              src="https://profe-santi-questions.s3.amazonaws.com/jerarquizacion-removebg-preview.png"
              alt="Jerarquizacion"
              width={500}
              height={500}
              className="mt-8"
            />
            </TabPanel>
            <TabPanel>
              <p className='mt-8'>Ejemplo</p>
            <Image
              src="https://profe-santi-questions.s3.amazonaws.com/completamiento-removebg-preview.png"
              alt="Completamiento"
              width={500}
              height={500}
              className="mt-8"
            />
            </TabPanel>

            <TabPanel>
              <p className='mt-8'>Ejemplo</p>
            <Image
              src="https://profe-santi-questions.s3.amazonaws.com/relacion_columnas-removebg-preview.png"
              alt="Relación de columnas"
              width={500}
              height={500}
              className="mt-8"
            />
            </TabPanel>

            <TabPanel>
              <p className='mt-8'>Ejemplo</p>
            <Image
              src="https://profe-santi-questions.s3.amazonaws.com/eleccion_elementos-removebg-preview.png"
              alt="Elección de elementos"
              width={500}
              height={500}
              className="mt-8"
            />
            </TabPanel>

            <TabPanel>
              <p className='mt-8'>Ejemplo</p>
            <Image
              src="https://profe-santi-questions.s3.amazonaws.com/decision-removebg-preview.png"
              alt="Decisión"
              width={500}
              height={500}
              className="mt-8"
            />
            </TabPanel>

      
      </Tabs>

      </div>

      <div className='mt-16'>
      <h2 className="text-xl font-bold text-[#F900BF]">2. Subir las imagenes en formato .png y con fondo transparente</h2>
      <p className='mt-8'> Para remover el background de una imagen y dejarla transparente pueden utilizar la siguiente herramienta:  <Link href="https://www.remove.bg/upload" legacyBehavior passHref>
  <a target="_blank" rel="noopener noreferrer" className='underline decoration-pink-500'>
    Background remover
  </a>
</Link>
     </p>

      </div>

    </div>


    )
}
