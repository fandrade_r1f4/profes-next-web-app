import React, {useState, useEffect} from 'react'
import axios from 'axios'
import { useRouter } from 'next/router'
import { Auth } from 'aws-amplify'
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCirclePlus, faXmark, faPenToSquare, faTrash } from '@fortawesome/free-solid-svg-icons'
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import Modal from 'react-modal';
import 'react-tabs/style/react-tabs.css';
import ClipLoader from 'react-spinners/ClipLoader'


const override = {
    position: 'fixed',
    left: '50%',
    top: '50%',
}

const customStyles = {
    content: {
      top: '50%',
      left: '50%',
      right: 'auto',
      bottom: 'auto',
      marginRight: '-50%',
      transform: 'translate(-50%, -50%)',
    },
  };
  

Modal.setAppElement('#__next');


 

const Topics = () => {

    const [userJwt, setUserJwt] = useState(null)

    const router = useRouter()

    const [isLoading, setIsLoading] = useState(false)

    const [signatures, setSignatures] = useState([]);

    const [units, setUnits] = useState([]);

    const [topics, setTopics] = useState([]);

    const [subtopics, setSubtopics] = useState([]);

    const [signatureSelected, setSignatureSelected] = useState({});
    const [unitSelected, setUnitSelected] = useState({});
    const [topicSelected, setTopicSelected] = useState({});

    const [modalIsOpen, setIsOpen] = useState(false);

    const [elementAction, setElementAction] = useState('');

    const [unitName, setUnitName] = useState(''); 
    const [unitNameError, setUnitNameError] = useState('');

    const [topicName, setTopicName] = useState('');
    const [topicNameError, setTopicNameError] = useState('');

    const [subtopicName, setSubtopicName] = useState('');
    const [subtopicNameError, setSubtopicNameError] = useState('');

    const [newUnitName, setNewUnitName] = useState('');
    const [newUnitNameError, setNewUnitNameError] = useState('');

    const [newTopicName, setNewTopicName] = useState('');
    const [newTopicNameError, setNewTopicNameError] = useState('');

    const [newSubtopicName, setNewSubtopicName] = useState('');
    const [newSubtopicNameError, setNewSubtopicNameError] = useState('');

    const [subtopicSelected, setSubtopicSelected] = useState('');

    const [dataHasChanged, setDataHasChanged] = useState(false);


    useEffect(() => {
        checkUser()
    }, [])

    async function checkUser() {
        try {
            const user = await Auth.currentAuthenticatedUser()
            setUserJwt(user.signInUserSession.idToken.jwtToken)
        } catch (e) {
            console.log(e);
            router.push('/auth')
        }
    }

    useEffect(() => {
        const getData = async () => {
            try {
                setIsLoading(true)

                let { data: signatures } = await axios.get(
                    `${process.env.NEXT_PUBLIC_AMAZON_URL}/${process.env.NEXT_PUBLIC_STAGE}/signatures`,
                    {
                        headers: {
                            'x-api-key': process.env.NEXT_PUBLIC_API_KEY,
                            Authorization: 'Bearer ' + userJwt,
                        },
                    }
                )


                let { data: units } = await axios.get(
                     `${process.env.NEXT_PUBLIC_AMAZON_URL}/${process.env.NEXT_PUBLIC_STAGE}/unit`,
                     {
                         headers: {
                             'x-api-key': process.env.NEXT_PUBLIC_API_KEY,
                             Authorization: 'Bearer ' + userJwt,
                         },
                     }
                 )


                 setUnits(units);



                let { data: topics } = await axios.get(
                     `${process.env.NEXT_PUBLIC_AMAZON_URL}/${process.env.NEXT_PUBLIC_STAGE}/topic`,
                     {
                         headers: {
                             'x-api-key': process.env.NEXT_PUBLIC_API_KEY,
                             Authorization: 'Bearer ' + userJwt,
                         },
                     }
                 )

                 setTopics(topics)
                 
                 


                let { data: subtopics } = await axios.get(
                     `${process.env.NEXT_PUBLIC_AMAZON_URL}/${process.env.NEXT_PUBLIC_STAGE}/subtopic`,
                     {
                         headers: {
                             'x-api-key': process.env.NEXT_PUBLIC_API_KEY,
                             Authorization: 'Bearer ' + userJwt,
                         },
                     }
                 )


                 setSubtopics(subtopics);

               let signaturesData = signatures.map(signature => ({
                id: signature.id,
                name: signature.name
               }))

               setSignatures(signaturesData);       
              
               let defaultUnit = {};
               let defaultTopic = {};
               if(signatureSelected && Object.keys(signatureSelected).length > 0){
                //instead of default, change for signature selected
             
                //console.log(unitSelected)
                if(unitSelected && Object.keys(unitSelected).length > 0){
                    setUnitSelected(unitSelected);
                    if(topicSelected && Object.keys(topicSelected).length > 0){
                        setTopicSelected(topicSelected);
                    } else {
                        defaultTopic = topics.filter(topic => topic.unitId === unitSelected.id)[0];
                        setTopicSelected(defaultTopic);
                    }
                  
                } else {
                    defaultUnit = units.filter(unit => unit.signatureId === signatureSelected.id)[0]
                    setUnitSelected(defaultUnit);
                    if(defaultUnit && Object.keys(defaultUnit).length > 0){
                        defaultTopic = topics.filter(topic => topic.unitId === defaultUnit.id)[0];
                        setTopicSelected(defaultTopic);
                    }
                }   

               } else {
                setSignatureSelected(signaturesData[0]);
                defaultUnit = units.filter(unit => unit.signatureId === signaturesData[0].id)[0]
                setUnitSelected(defaultUnit);
                if(defaultUnit && Object.keys(defaultUnit).length > 0){
                    defaultTopic = topics.filter(topic => topic.unitId === defaultUnit.id)[0];
                    setTopicSelected(defaultTopic);
                }
               
               }
              
                
               setIsLoading(false);

      
                    
            } catch (e) {
                setIsLoading(false);
                console.log(e)
            }
        }
        if (userJwt) getData()
    }, [userJwt, dataHasChanged])

    function openModal() {
        setIsOpen(true);
     
    }

    function closeModal() {
        setIsOpen(false);
    }


    const addUnit = async () => {
        if(!unitName){
            setUnitNameError('Favor de ingresar el nombre de la unidad')
            return false;
        }
        try{
            setIsLoading(true);
           const {data: unitData} =  await axios.post(
                `${process.env.NEXT_PUBLIC_AMAZON_URL}/${process.env.NEXT_PUBLIC_STAGE}/unit`,
                {
                    name:unitName,
                    signatureId: signatureSelected.id
                },
                {
                    headers: {
                        'x-api-key': process.env.NEXT_PUBLIC_API_KEY,
                        Authorization: 'Bearer ' + userJwt,
                    },
                }
            )
            setUnitSelected(unitData);
            setIsLoading(false);
            closeModal();
            toast.success(`Unidad agregada correctamente 🙂`)
            setDataHasChanged(!dataHasChanged)
            //setUnitSelected({});
            setUnitName('');

        }
        catch(e){
            closeModal();
            toast.error(
                `Hubo un problema agregando la unidad. Favor de intentar más tarde 🤔`
            )
            setIsLoading(false);
            setUnitName('');
            console.log(e);
        }
    }


    const addTopic = async () => {
        if(!topicName){
            setTopicNameError('Favor de ingresar el nombre del tema')
            return false;
        }
        /*console.log(unitSelected);
        console.log(topicName)*/
        //console.log(unitSelected.id);
        try{
            setIsLoading(true);
            const {data: topicData} = await axios.post(
                `${process.env.NEXT_PUBLIC_AMAZON_URL}/${process.env.NEXT_PUBLIC_STAGE}/topic`,
                {
                    name:topicName,
                    unitId: unitSelected.id
                },
                {
                    headers: {
                        'x-api-key': process.env.NEXT_PUBLIC_API_KEY,
                        Authorization: 'Bearer ' + userJwt,
                    },
                }
            )
            setTopicSelected(topicData);
            setIsLoading(false);
            closeModal();
            toast.success(`Tema agregado correctamente 🙂`)
            setDataHasChanged(!dataHasChanged)
            setUnitName('');
            setTopicName('');

        }
        catch(e){
            closeModal();
            toast.error(
                `Hubo un problema agregando el tema. Favor de intentar más tarde 🤔`
            )
            setIsLoading(false);
            setUnitName('');
            setTopicName('');
            console.log(e);
        }
        
    }


    const addSubtopic = async () => {
        if(!subtopicName){
            setSubtopicNameError('Favor de ingresar el nombre del subtema')
            return false;
        }

        try{
            setIsLoading(true);
            const {data: subTopicData} = await axios.post(
                `${process.env.NEXT_PUBLIC_AMAZON_URL}/${process.env.NEXT_PUBLIC_STAGE}/subtopic`,
                {
                    name:subtopicName,
                    topicsId: topicSelected.id
                },
                {
                    headers: {
                        'x-api-key': process.env.NEXT_PUBLIC_API_KEY,
                        Authorization: 'Bearer ' + userJwt,
                    },
                }
            )
            setSubtopicSelected(subTopicData);
            setIsLoading(false);
            closeModal();
            toast.success(`Subtema agregado correctamente 🙂`)
            setDataHasChanged(!dataHasChanged)
            setUnitName('');
            setTopicName('');
            setSubtopicName('');

        }
        catch(e){
            closeModal();
            toast.error(
                `Hubo un problema agregando el subtema. Favor de intentar más tarde 🤔`
            )
            setIsLoading(false);
            setUnitName('');
            setTopicName('');
            console.log(e);
        }
       
        /*console.log(unitSelected);
        console.log(topicSelected)
        console.log(subtopicName);*/
        closeModal();
    }

    const editUnit = async () => {
        
        if(!newUnitName){
            setNewUnitNameError('Favor de ingresar el nuevo nombre de la unidad')
            return false; 
        }

        try{
            setIsLoading(true);
             await axios.put(
                `${process.env.NEXT_PUBLIC_AMAZON_URL}/${process.env.NEXT_PUBLIC_STAGE}/unit/${unitSelected.id}`,
                {
                    name: newUnitName,
                },
                {
                    headers: {
                        'x-api-key': process.env.NEXT_PUBLIC_API_KEY,
                        Authorization: 'Bearer ' + userJwt,
                    },
                }
            )
            setIsLoading(false);  
            closeModal();
            toast.success(`Unidad editada correctamente 🙂`)
            setDataHasChanged(!dataHasChanged)
            setNewUnitName('');


        }
        catch(e){
            closeModal();
            toast.error(
                `Hubo un problema editando la unidad. Favor de intentar más tarde 🤔`
            )
            setNewUnitName('');
            setIsLoading(false);
            console.log(e);
        }
        
    }

    const editTopic = async () => {
        
        if(!newTopicName){
            setNewTopicNameError('Favor de ingresar el nuevo nombre del tema')
            return false; 
        }

        try{
            setIsLoading(true);
             await axios.put(
                `${process.env.NEXT_PUBLIC_AMAZON_URL}/${process.env.NEXT_PUBLIC_STAGE}/topic/${topicSelected.id}`,
                {
                    name: newTopicName,
                },
                {
                    headers: {
                        'x-api-key': process.env.NEXT_PUBLIC_API_KEY,
                        Authorization: 'Bearer ' + userJwt,
                    },
                }
            )
            setIsLoading(false);  
            closeModal();
            toast.success(`Tema editado correctamente 🙂`)
            setDataHasChanged(!dataHasChanged)
            setNewUnitName('');
            setNewTopicName('');


        }
        catch(e){
            closeModal();
            toast.error(
                `Hubo un problema editando el tema. Favor de intentar más tarde 🤔`
            )
            setNewUnitName('');
            setNewTopicName('');
            setIsLoading(false);
            console.log(e);
        }
        
    }


    const editSubtopic = async () => {
        
        if(!newSubtopicName){
            setNewSubtopicNameError('Favor de ingresar el nuevo nombre del subtema')
            return false; 
        }

        console.log(subtopicSelected);

        try{
            setIsLoading(true);
             await axios.put(
                `${process.env.NEXT_PUBLIC_AMAZON_URL}/${process.env.NEXT_PUBLIC_STAGE}/subtopic/${subtopicSelected.id}`,
                {
                    name: newSubtopicName,
                },
                {
                    headers: {
                        'x-api-key': process.env.NEXT_PUBLIC_API_KEY,
                        Authorization: 'Bearer ' + userJwt,
                    },
                }
            )
            setIsLoading(false);  
            closeModal();
            toast.success(`Subtema editado correctamente 🙂`)
            setDataHasChanged(!dataHasChanged)
            setNewUnitName('');
            setNewTopicName('');
            setNewSubtopicName('');


        }
        catch(e){
            closeModal();
            toast.error(
                `Hubo un problema editando el subtema. Favor de intentar más tarde 🤔`
            )
            setNewUnitName('');
            setNewTopicName('');
            setNewSubtopicName('');
            setIsLoading(false);
            console.log(e);
        }
        
    }

    const deleteUnit = async () => {
        try{
            setIsLoading(true);
             await axios.delete(
                `${process.env.NEXT_PUBLIC_AMAZON_URL}/${process.env.NEXT_PUBLIC_STAGE}/unit/${unitSelected.id}`,
                {
                    headers: {
                        'x-api-key': process.env.NEXT_PUBLIC_API_KEY,
                        Authorization: 'Bearer ' + userJwt,
                    },
                }
            )
            setUnitSelected({})
            setIsLoading(false);  
            closeModal();
            toast.success(`Unidad eliminada correctamente 🙂`)
            setDataHasChanged(!dataHasChanged)


        }
        catch(e){
            closeModal();
            toast.error(
                `Hubo un problema eliminando la unidad. Favor de intentar más tarde 🤔`
            )
            setIsLoading(false);
            console.log(e);
        }
    }

    const deleteTopic = async () => {
        try{
            setIsLoading(true);
             await axios.delete(
                `${process.env.NEXT_PUBLIC_AMAZON_URL}/${process.env.NEXT_PUBLIC_STAGE}/topic/${topicSelected.id}`,
                {
                    headers: {
                        'x-api-key': process.env.NEXT_PUBLIC_API_KEY,
                        Authorization: 'Bearer ' + userJwt,
                    },
                }
            ) 
            


            setTopicSelected({});
            setIsLoading(false);  
            closeModal();
            toast.success(`Tema eliminado correctamente 🙂`)
            setDataHasChanged(!dataHasChanged)
       


        }
        catch(e){
            closeModal();
            toast.error(
                `Hubo un problema eliminando el tema. Favor de intentar más tarde 🤔`
            )
            setIsLoading(false);
            console.log(e);
        }
    }

    const deleteSubtopic = async () => {
        try{
            setIsLoading(true);
             await axios.delete(
                `${process.env.NEXT_PUBLIC_AMAZON_URL}/${process.env.NEXT_PUBLIC_STAGE}/subtopic/${subtopicSelected.id}`,
                {
                    headers: {
                        'x-api-key': process.env.NEXT_PUBLIC_API_KEY,
                        Authorization: 'Bearer ' + userJwt,
                    },
                }
            )
            setSubtopicSelected({})
            setIsLoading(false);  
            closeModal();
            toast.success(`Subtema eliminado correctamente 🙂`)
            setDataHasChanged(!dataHasChanged)

        }
        catch(e){
            closeModal();
            toast.error(
                `Hubo un problema eliminando el subtema. Favor de intentar más tarde 🤔`
            )
            setIsLoading(false);
            console.log(e);
        }
    }

    console.log(unitSelected)

  return (
    <>
    { isLoading  &&
      <ClipLoader
          color={'#ec4899'}
          size={40}
          aria-label="Loading Spinner"
          data-testid="loader"
          loading={isLoading}
          cssOverride={override}
      /> 
    }

    {/**Add units.length > 0 */}
     {signatures.length > 0 && 
      
    <div className="container rounded-lg text-[#F900BF] bg-gray-100  h-auto  mt-8 py-6 mb-8 px-16 max-w-4xl" >
     <ToastContainer />

    <div id='modal'>
    <Modal
        isOpen={modalIsOpen}
        onRequestClose={closeModal}
        style={customStyles}
        contentLabel="modal"
      >
        
        {elementAction === 'add_unit' && 
            <div className='w-96'>
                <div className='flex justify-between'>
                    <p className='mt-2 text-violet-800 font-bold'>{signatureSelected.name}</p>
                    <FontAwesomeIcon icon={faXmark} className='cursor-pointer' onClick={closeModal} />
                </div>
                <h1 className='mt-2 text-blue-800'>Agregar unidad</h1>
                <div className='mt-4'>
                    <label htmlFor="unit" className="block mb-2 text-sm font-medium text-gray-900">Nombre de la unidad
                    <input type="text" id="unit" className="border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5" onChange={(e) => {
                        setUnitName(e.target.value);
                        setUnitNameError('');
                    }} value={unitName}/>
                    </label>
                </div>
                <div className="mt-2">
                    {unitNameError && (
                        <p className="text-rose-600">
                            {unitNameError}
                        </p>
                    )}
                </div>
                <div className='mt-3'>
                <button className= {`${isLoading ? 'bg-slate-300' : 'bg-pink-500 hover:bg-pink-600 '}  text-white font-bold py-2 px-4 rounded w-full`} disabled={isLoading ? true: false}  onClick={addUnit}>
                    Agregar
                </button>
                </div>
            </div>
        }

        {elementAction === 'add_topic' &&  unitSelected &&
            <div className='w-96'>
                <div className='flex justify-between'>
                    <p className='mt-2 text-violet-800 font-bold'>{signatureSelected.name}</p>
                    <FontAwesomeIcon icon={faXmark} className='cursor-pointer' onClick={closeModal} />
                </div>
                <h1 className='mt-2 text-blue-800'>{unitSelected.name}</h1>
                <h1 className='mt-2 text-blue-800'>Agregar tema</h1>
                <div className='mt-4'>
                    <label htmlFor="unit" className="block mb-2 text-sm font-medium text-gray-900">Nombre del tema
                    <input type="text" id="unit" className="border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5" onChange={(e) => {
                        setTopicName(e.target.value);
                        setTopicNameError('');
                    }} value={topicName}/>
                    </label> 
                </div>
                <div className="mt-2">
                    {topicNameError && (
                        <p className="text-rose-600">
                            {topicNameError}
                        </p>
                    )}
                </div>
                <div className='mt-3'>
                <button className="bg-pink-500	hover:bg-pink-600 text-white font-bold py-2 px-4 rounded w-full" onClick={addTopic}>
                    Agregar
                </button>
                </div>
            </div>
        }
        {elementAction === 'add_subtopic' && topicSelected && Object.keys(topicSelected).length > 0 && unitSelected && Object.keys(unitSelected).length > 0 &&

            <div className='w-96'>
                <div className='flex justify-between'>
                    <p className='mt-2 text-violet-800 font-bold'>{signatureSelected.name}</p>
                    <FontAwesomeIcon icon={faXmark} className='cursor-pointer' onClick={closeModal} />
                </div>
                <h1 className='mt-2 text-blue-800'>{unitSelected.name}</h1>
                <h1 className='mt-2 text-blue-800'>{topicSelected.name}</h1>
                <h1 className='mt-2 text-blue-800'>Agregar subtema</h1>
                <div className='mt-4'>
                    <label htmlFor="unit" className="block mb-2 text-sm font-medium text-gray-900">Nombre del subtema
                    <input type="text" id="unit" className="border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5" onChange={(e) => {
                        setSubtopicName(e.target.value);
                        setSubtopicNameError('');
                    }} value={subtopicName}/>
                    </label> 
                </div>
                <div className="mt-2">
                    {subtopicNameError && (
                        <p className="text-rose-600">
                            {subtopicNameError}
                        </p>
                    )}
                </div>
                <div className='mt-3'>
                <button className= {`${isLoading ? 'bg-slate-300' : 'bg-pink-500 hover:bg-pink-600 '}  text-white font-bold py-2 px-4 rounded w-full`} disabled={isLoading ? true: false} onClick={addSubtopic}>
                    Agregar
                </button>
                </div>
            </div>
        }

        {elementAction === 'edit_unit' && 
            <div className='w-96'>
                <div className='flex justify-between'>
                    <p className='mt-2 text-violet-800 font-bold'>{signatureSelected.name}</p>
                    <FontAwesomeIcon icon={faXmark} className='cursor-pointer' onClick={closeModal} />
                </div>
                <h1 className='mt-2 text-blue-800'>Editar Unidad</h1>
                <div className='mt-4'>
                    <label htmlFor="unit" className="block mb-2 text-sm font-medium text-gray-900">Nombre de la unidad
                    <input type="text" id="unit" className="border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5" onChange={(e) => {
                        setNewUnitName(e.target.value);
                        setNewUnitNameError('');
                    
                    }} value={newUnitName}/>
                    </label> 
                </div>
                <div className="mt-2">
                    {newUnitNameError && (
                        <p className="text-rose-600">
                            {newUnitNameError}
                        </p>
                    )}
                </div>
                <div className='mt-3'>
                <button className= {`${isLoading ? 'bg-slate-300' : 'bg-pink-500 hover:bg-pink-600'}   text-white font-bold py-2 px-4 rounded w-full`} disabled={isLoading ? true: false}  onClick={editUnit}>
                    Editar
                </button>
                </div>
            </div>
        }

        {elementAction === 'edit_topic' && 
            <div className='w-96'>
                <div className='flex justify-between'>
                    <p className='mt-2 text-violet-800 font-bold'>{signatureSelected.name}</p>
                    <FontAwesomeIcon icon={faXmark} className='cursor-pointer' onClick={closeModal} />
                </div>
                <h1 className='mt-2 text-blue-800'>{unitSelected.name}</h1>
                <h1 className='mt-2 text-blue-800'>Editar Tema</h1>
                <div className='mt-4'>
                    <label htmlFor="unit" className="block mb-2 text-sm font-medium text-gray-900">Nombre del tema
                    <input type="text" id="unit" className="border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5" onChange={(e) => {
                        setNewTopicName(e.target.value);
                        setNewTopicNameError('');    
                    }} value={newTopicName}/>
                    </label> 
                </div>
                <div className="mt-2">
                    {newTopicNameError && (
                        <p className="text-rose-600">
                            {newTopicNameError}
                        </p>
                    )}
                </div>
                <div className='mt-3'>
                <button className= {`${isLoading ? 'bg-slate-300' : 'bg-pink-500 hover:bg-pink-600'}   text-white font-bold py-2 px-4 rounded w-full`} disabled={isLoading ? true: false} onClick={editTopic}>
                    Editar
                </button>
                </div>
            </div>
        }

    {elementAction === 'edit_subtopic' &&  topicSelected && Object.keys(topicSelected).length > 0 &&
            <div className='w-96'>
                <div className='flex justify-between'>
                    <p className='mt-2 text-violet-800 font-bold'>{signatureSelected.name}</p>
                    <FontAwesomeIcon icon={faXmark} className='cursor-pointer' onClick={closeModal} />
                </div>
                <h1 className='mt-2 text-blue-800'>{unitSelected.name}</h1>
                <h1 className='mt-2 text-blue-800'>{topicSelected.name}</h1>

                <h1 className='mt-2 text-blue-800'>Editar Subtema</h1>
                <div className='mt-4'>
                    <label htmlFor="unit" className="block mb-2 text-sm font-medium text-gray-900">Nombre del subtema
                    <input type="text" id="unit" className="border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5" onChange={(e) => {
                        setNewSubtopicName(e.target.value);
                        setNewSubtopicNameError('');    
                    }} value={newSubtopicName}/>
                    </label> 
                </div>
                <div className="mt-2">
                    {newSubtopicNameError && (
                        <p className="text-rose-600">
                            {newSubtopicNameError}
                        </p>
                    )}
                </div>
                <div className='mt-3'>
                <button className= {`${isLoading ? 'bg-slate-300' : 'bg-pink-500 hover:bg-pink-600'}   text-white font-bold py-2 px-4 rounded w-full`} disabled={isLoading ? true: false} onClick={editSubtopic}>
                    Editar
                </button>
                </div>
            </div>
    }

    {elementAction === 'delete_unit' && unitSelected && 
                <div className='w-96'>
                <div className='flex justify-between'>
                    <p className='mt-2 text-violet-800 font-bold'>{signatureSelected.name}</p>
                    <FontAwesomeIcon icon={faXmark} className='cursor-pointer' onClick={closeModal} />
                </div>
                <h1 className='mt-2 text-blue-800'>Eliminar unidad</h1>
                {<p className='mt-4'>
                    ¿Estás seguro que deseas eliminar la unidad {unitSelected.name}?
                </p>}
                <div className='mt-6 flex justify-between'>
                <button className= {`${isLoading ? 'bg-slate-300' : 'bg-red-600 hover:bg-red-700 ' } text-white font-bold py-2 px-4 rounded w-40`}  disabled={isLoading ? true: false} onClick={deleteUnit}>
                    Eliminar
                </button>
                <button className="bg-purple-600 hover:bg-purple-700 text-white font-bold py-2 px-4 rounded w-40" onClick={closeModal}>
                    Cancelar
                </button>
                </div>
            </div>
    }

    {elementAction === 'delete_topic' && topicSelected &&
            <div className='w-96'>
                <div className='flex justify-between'>
                    <p className='mt-2 text-violet-800 font-bold'>{signatureSelected.name}</p>
                    <FontAwesomeIcon icon={faXmark} className='cursor-pointer' onClick={closeModal} />
                </div>
                <h1 className='mt-2 text-blue-800'>{unitSelected.name}</h1>
                <h1 className='mt-2 text-blue-800'>Eliminar tema</h1>
        
                {<p className='mt-4'>
                    ¿Estás seguro que deseas eliminar el tema {topicSelected.name}?
                </p>}
                <div className='mt-6 flex justify-between'>
                <button className= {`${isLoading ? 'bg-slate-300' : 'bg-red-600 hover:bg-red-700 ' } text-white font-bold py-2 px-4 rounded w-40`}  disabled={isLoading ? true: false} onClick={deleteTopic}>
                    Eliminar
                </button>
                <button className="bg-purple-600 hover:bg-purple-700 text-white font-bold py-2 px-4 rounded w-40" onClick={closeModal}>
                    Cancelar
                </button>
                </div>
            </div>
    }

    {elementAction === 'delete_subtopic' &&  topicSelected && Object.keys(topicSelected).length > 0 &&
             <div className='w-96'>
             <div className='flex justify-between'>
                 <p className='mt-2 text-violet-800 font-bold'>{signatureSelected.name}</p>
                 <FontAwesomeIcon icon={faXmark} className='cursor-pointer' onClick={closeModal} />
             </div>
             <h1 className='mt-2 text-blue-800'>{unitSelected.name}</h1>
             <h1 className='mt-2 text-blue-800'>{topicSelected.name}</h1>

             <h1 className='mt-2 text-blue-800'>Eliminar subtema</h1>

             
             {<p className='mt-4'>
                    ¿Estás seguro que deseas eliminar el subtema {subtopicSelected.name}?
                </p>}
                <div className='mt-6 flex justify-between'>
                <button className= {`${isLoading ? 'bg-slate-300' : 'bg-red-600 hover:bg-red-700 ' } text-white font-bold py-2 px-4 rounded w-40`}  disabled={isLoading ? true: false} onClick={deleteSubtopic}>
                    Eliminar
                </button>
                <button className="bg-purple-600 hover:bg-purple-700 text-white font-bold py-2 px-4 rounded w-40" onClick={closeModal}>
                    Cancelar
                </button>
                </div>
            
         </div>
    }


    </Modal>  
    </div>    


        <h1>Temario</h1>

       


        <Tabs className='mt-8'>
            <TabList>
                {signatures.map((signature) => (
                      <Tab onClick={() => {
                        setSignatureSelected(signature)
                        const defaultUnit = units.filter(unit => unit.signatureId === signatures.filter(s => s.id === signature.id)[0].id)[0];
                        if(defaultUnit && Object.keys(defaultUnit).length > 0){
                            setUnitSelected(defaultUnit)
                            const defaultTopic = topics.filter(topic => topic.unitId === defaultUnit.id)[0];
                            setTopicSelected(defaultTopic);
                        } else {
                            setUnitSelected({})
                            setTopicSelected({});
                        }
                                             
                      }}
                      key={signature.id}
                      >{signature.name}</Tab>
                ))}
              
            </TabList>

            {signatures.map((signature) => (
                
                    <TabPanel key={signature.id}>
                        <div className='flex justify-between'>
                            <div className='flex flex-col'>
                                <span>Unidades <FontAwesomeIcon icon={faCirclePlus} className='ml-2 cursor-pointer' onClick={() => {
                                    openModal()
                                    setElementAction('add_unit')
                                }} /></span>
                                <ul className='mt-4'>
                                    {units.filter((unit) => unit.signatureId === signatureSelected.id).map((unit) => <li key={unit.id}   onClick={() => {
                                        setUnitSelected(unit);
                                        const defaultTopic = topics.filter(topic => topic.unitId === unit.id)[0];
                                        setTopicSelected(defaultTopic);
                                        
                                    }} className={`mt-2 text-indigo-800	cursor-pointer ${unitSelected.id === unit.id ? 'underline underline-offset-4' : ''}`}>{unit.name}    
                                    <FontAwesomeIcon icon={faPenToSquare} className=' ml-4 cursor-pointer' onClick={() => {
                                        setNewUnitName(unit.name)
                                        setElementAction('edit_unit')
                                        setNewUnitNameError('');
                                        openModal();
                                        
                                    }} size="xs"/>
                                    <FontAwesomeIcon icon={faTrash} className=' ml-2 cursor-pointer' onClick={() => {
                                        setElementAction('delete_unit')
                                        openModal();
                                    }} size="xs"/>

                                    </li>)}
                                </ul>
                            </div>
                            <div className='flex flex-col'>
                                {unitSelected && Object.keys(unitSelected).length > 0 &&
                                <>
                                <span>Temas <FontAwesomeIcon icon={faCirclePlus} className='ml-2 cursor-pointer' onClick={() => {
                                    openModal()
                                    setElementAction('add_topic')
                                }} /></span>
                                {topicSelected && Object.keys(topicSelected) && 
                                <ul className='mt-4'>
                                    {topics.filter((topic) => topic.unitId === unitSelected.id).map((topic) => <li key={topic.id} className={`mt-2 text-indigo-800	cursor-pointer ${topicSelected.id === topic.id ? 'underline underline-offset-4' : ''}`} onClick={() => setTopicSelected(topic)}>{topic.name}
                                    <FontAwesomeIcon icon={faPenToSquare} className=' ml-4 cursor-pointer' onClick={() => {
                                         setNewTopicName(topic.name)
                                         setElementAction('edit_topic')
                                         setNewTopicNameError('');
                                         openModal();
                                    }} size="xs"/>
                                    <FontAwesomeIcon icon={faTrash} className=' ml-2 cursor-pointer' onClick={() => {
                                        setElementAction('delete_topic')
                                        openModal();
                                    }} size="xs"/></li>)}
                                </ul> }
                                </>}
                            </div>
                            <div className='flex flex-col'>
                                {topicSelected && Object.keys(topicSelected).length > 0 && unitSelected && Object.keys(unitSelected).length > 0 &&
                                <>
                                <span>Subtemas <FontAwesomeIcon icon={faCirclePlus} className='ml-2 cursor-pointer' onClick={() => {
                                    openModal()
                                    setElementAction('add_subtopic')
                                }} /></span>
                                <ul className='mt-4'>
                                    {topicSelected ? subtopics.filter((subtopic) => subtopic.topicsId === topicSelected.id).map((subtopic) => <li key={subtopic.id} className='mt-2 text-indigo-800' onClick={() => setSubtopicSelected(subtopic)}>{subtopic.name}
                                    <FontAwesomeIcon icon={faPenToSquare} className=' ml-4 cursor-pointer'  onClick={() => {
                                         setNewSubtopicName(subtopic.name)
                                         setElementAction('edit_subtopic')
                                         setNewSubtopicNameError('');
                                         openModal();
                                    }} size="xs"/>
                                    <FontAwesomeIcon icon={faTrash} className=' ml-2 cursor-pointer' onClick={() => {
                                        setElementAction('delete_subtopic')
                                        setSubtopicSelected(subtopic)
                                        openModal();
                                    }} size="xs"/>
                                    </li>) : <li></li>}
                                </ul>
                            </>
}
                            </div>
                        </div>
                        
                    </TabPanel>
                
            ))}
         
         
        </Tabs>
        

        </div>
    }

      
    
     </>
        
  )
}

export default Topics