import '../styles/globals.css'
import Link from 'next/link'

function MyApp({ Component, pageProps }) {
    return (
        <div>
            <nav className="py-4 px-12 flex bg-[#F900BF] ">
                <Link legacyBehavior href="/">
                    <a className="text-white leading-6 font-medium hover:text-[#FF85B3] transition-colors duration-200 py-2">
                        <p>Inicio</p>
                    </a>
                </Link>
                <Link legacyBehavior href="/topics">
                    <a className="text-white leading-6 font-medium hover:text-[#FF85B3] transition-colors duration-200 ml-10 py-2">
                        <p>Temario</p>
                    </a>
                </Link>
                <Link legacyBehavior href="/add-questions">
                    <a className="text-white leading-6 font-medium hover:text-[#FF85B3] transition-colors duration-200 py-2 ml-10">
                        <p>Agregar preguntas</p>
                    </a>
                </Link>
                <Link legacyBehavior href="/view-questions">
                    <a className="text-white leading-6 font-medium hover:text-[#FF85B3] transition-colors duration-200 py-2 ml-10">
                        <p>Ver preguntas</p>
                    </a>
                </Link>
                <Link legacyBehavior href="/profile">
                    <a className="text-white leading-6 font-medium hover:text-[#FF85B3] transition-colors duration-200 py-2 ml-10">
                        <p>Perfil</p>
                    </a>
                </Link>
            </nav>
            <Component {...pageProps} />
        </div>
    )
}

export default MyApp
