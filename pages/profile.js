import { useState, useEffect } from 'react'
import { Auth } from 'aws-amplify'
import { useRouter } from 'next/router'

function Profile({ setUiState }) {
    const [user, setUser] = useState(null)
    const router = useRouter()

    useEffect(() => {
        checkUser()
    }, [])
    async function checkUser() {
        try {
            const user = await Auth.currentAuthenticatedUser()
            setUser(user.attributes)
        } catch (e) {
            router.push('/auth')
        }
    }
    if (!user) return null
    return (
        <div className="container h-auto  mt-8 py-6 mb-8 px-16 max-w-3xl">
            <p className="text-xl font-black text-[#F900BF]">
                Bienvenido(a), {user.email}
            </p>
            <button
                onClick={() => {
                    Auth.signOut()
                    router.push('/auth')
                }}
                className="text-white bg-[#9900F0] w-full font-medium rounded-lg text-sm px-5 py-2.5 text-center mr-2 mt-4"
            >
                Salir
            </button>
        </div>
    )
}

export default Profile
