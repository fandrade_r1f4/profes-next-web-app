import { useState, useEffect } from 'react'
import { Auth } from 'aws-amplify'
import { v4 as uuidv4 } from 'uuid'
import { useRouter } from 'next/router'
import ClipLoader from 'react-spinners/ClipLoader'
import axios from 'axios'
import Select from 'react-select'
import Toggle from 'react-toggle'
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import 'react-toggle/style.css'
import Column from '../components/Column'

const BUCKET_URL = 'https://profe-santi-questions.s3.amazonaws.com/'

const override = {
    position: 'fixed',
    left: '50%',
    top: '50%',
}

function AddQuestions({ setUiState }) {
    const [userJwt, setUserJtw] = useState('')
    
    const router = useRouter()

   

    const kindQuestion = [
        { value: 'directQuestioning', label: 'Cuestionamiento Directo' },
        { value: 'completing', label: 'Completamiento' },
        { value: 'justification', label: 'Decisión o justificación de experto' },
        { value: 'columnsRelation', label: 'Relacion de Columnas' },
    ]



    const [option1, setOption1] = useState({
        option: 1,
        letter: 'A',
        value: '',
    })

    const [option2, setOption2] = useState({
        option: 2,
        letter: 'B',
        value: '',
    })

    const [option3, setOption3] = useState({
        option: 3,
        letter: 'C',
        value: '',
    })

    const [option4, setOption4] = useState({
        option: 4,
        letter: 'D',
        value: '',
    })

    const correctAnswers = [
        { value: 1, label: 'A' },
        { value: 2, label: 'B' },
        { value: 3, label: 'C' },
        { value: 4, label: 'D' },
    ]

    const difficulty = [
        { value: 'easy', label: 'Fácil' },
        { value: 'medium', label: 'Media' },
        { value: 'difficult', label: 'Difícil' },
    ]


    const [signatures, setSignatures] = useState([])

    const [units, setUnits] = useState([]);
    const [unitsSelect, setUnitsSelect] = useState([]);

    const [topics, setTopics] = useState([]);
    const [topicsSelect, setTopicsSelect] = useState([]);

    const [subtopics, setSubtopics] = useState([]);
    const [subtopicsSelect, setSubtopicsSelect] = useState([]);


    const [selectedSignature, setSelectedSignature] = useState(signatures[0])

    const [selectedUnit, setSelectedUnit] = useState(units[0])
    const [selectedTopic, setSelectedTopic] = useState(topics[0])
    const [selectedSubtopic, setSelectedSubtopic] = useState(subtopics[0])

    const [selectedKindQuestion, setSelectedKindQuestion] = useState(
        kindQuestion[0]
    )

    const [selectedCorrectAnswer, setSelectedCorrectAnswer] = useState(
        correctAnswers[0]
    )
    const [selectedDifficulty, setSelectedDifficulty] = useState(difficulty[0])

    const [hasImageText, setHasImageText] = useState(false)

    const [textQuestion, setTextQuestion] = useState('')
    const [textUploadedFile, setTextUploadedFile] = useState()
    const [textQuestionError, setTextQuestionError] = useState('')

    const [hasImageSubtext, setHasImageSubtext] = useState(false)

    const [subtextQuestion, setSubtextQuestion] = useState('')
    const [subtextUploadedFile, setSubtextUploadedFile] = useState('')

    const [optionOneError, setOptionOneError] = useState('')
    const [optionTwoError, setOptionTwoError] = useState('')
    const [optionThreeError, setOptionThreeError] = useState('')
    const [optionFourError, setOptionFourError] = useState('')

    const [isLoading, setIsLoading] = useState(false)

    const [hasImageOptionOne, setHasImageOptionOne] = useState(false)
    const [optionOneUploadedFile, setOptionOneUploadedFile] = useState('')

    const [hasImageOptionTwo, setHasImageOptionTwo] = useState(false)
    const [optionTwoUploadedFile, setOptionTwoUploadedFile] = useState('')

    const [hasImageOptionThree, setHasImageOptionThree] = useState(false)
        
    const [optionThreeUploadedFile, setOptionThreeUploadedFile] = useState('')

    const [hasImageOptionFour, setHasImageOptionFour] = useState(false)
    const [optionFourUploadedFile, setOptionFourUploadedFile] = useState('')

    
    const [titleColumnA, setTitleColumnA] = useState('');
    const [errorTitleColumnA, setErrorTitleColumnA] = useState('');    
    const [columnAOptions, setColumnAOptions] = useState([]);
    const [errorsIdsColumnA, setErrorsIdsColumnA] = useState([]);
    const [totalOptionsColumnA, setTotalOptionsColumnA] = useState(3); 



    const [titleColumnB, setTitleColumnB] = useState('');
    const [errorTitleColumnB, setErrorTitleColumnB] = useState('');
    const [columnBOptions, setColumnBOptions] = useState([]);
    const [errorsIdsColumnB, setErrorsIdsColumnB] = useState([]);
    const [totalOptionsColumnB, setTotalOptionsColumnB] = useState(3); 

    const [cleanColumnsForm, setCleanColumnsForm] = useState(false);


    const [videoQuestionUploadedFile, setVideoQuestionUploadedFile] = useState('')


    const [imageQuestionUploadedFile, setImageQuestionUploadedFile] = useState('')


    const [textExcercise, setTextExcercise] = useState('');
    const [textExcerciseError, setTextExcerciseError] = useState('');
    const [subtextExcercise, setSubTextExcercise] = useState('');

    const [optionOneExcercise, setOptionOneExcercise] = useState({
        option: 1,
        letter: 'A',
        value: '',
    })

    const [optionTwoExcercise, setOptionTwoExcercise] = useState({
        option: 2,
        letter: 'B',
        value: '',
    })

    const [optionThreeExcercise, setOptionThreeExcercise] = useState({
        option: 3,
        letter: 'C',
        value: '',
    })

    const [optionFourExcercise, setOptionFourExcercise] = useState({
        option: 4,
        letter: 'D',
        value: '',
    })

    const [optionOneExcerciseError, setOptionOneExcerciseError] = useState('');
    const [optionTwoExcerciseError, setOptionTwoExcerciseError] = useState('');
    const [optionThreeExcerciseError, setOptionThreeExcerciseError] = useState('');
    const [optionFourExcerciseError, setOptionFourExcerciseError] = useState('');

    const correctAnswersExcercise = [
        { value: 1, label: 'A' },
        { value: 2, label: 'B' },
        { value: 3, label: 'C' },
        { value: 4, label: 'D' },
    ]

    const [selectedCorrectAnswerExcercise, setSelectedCorrectAnswerExcercise] = useState(
        correctAnswersExcercise[0]
    )


    useEffect(() => {
        checkUser()
    }, [])

    async function checkUser() {
        try {
            const user = await Auth.currentAuthenticatedUser()
            //console.log(user);
            setUserJtw(user.signInUserSession.idToken.jwtToken)
        } catch (e) {
            console.log(e)
            router.push('/auth')
        }
    }

    /*useEffect(() => {
        if (userJwt) getSignatures()
    }, [userJwt])*/

   /* async function getSignatures() {
        try {
            setIsLoading(true)
            let { data: signatures } = await axios.get(
                `${process.env.NEXT_PUBLIC_AMAZON_URL}/${process.env.NEXT_PUBLIC_STAGE}/signatures`,
                {
                    headers: {
                        'x-api-key': process.env.NEXT_PUBLIC_API_KEY,
                        Authorization: 'Bearer ' + userJwt,
                    },
                }
            )

            signatures = signatures.map((signature) => ({
                ...signature,
                value: signature.name,
                label: signature.name,
            }))
            setSelectedSignature(signatures[0])
            setSignatures(signatures)

            setIsLoading(false)
        } catch (e) {
            console.log(e)
        }
    }*/

    useEffect(() => {
        const getData = async () => {
            try {
                setIsLoading(true)

                let { data: signatures } = await axios.get(
                   // `${process.env.NEXT_PUBLIC_AMAZON_URL}/${process.env.NEXT_PUBLIC_STAGE}/signatures`,
                   'https://mockend.com/franky358/mockend-api/signatures',
                    /*{
                        headers: {
                            'x-api-key': process.env.NEXT_PUBLIC_API_KEY,
                            Authorization: 'Bearer ' + userJwt,
                        },
                    }*/
                )


                signatures = signatures.map((signature) => ({
                    ...signature,
                    value: signature.name,
                    label: signature.name,
                }))
                setSignatures(signatures)
                setSelectedSignature(signatures[0])

                let { data: units } = await axios.get(
                    // `${process.env.NEXT_PUBLIC_AMAZON_URL}/${process.env.NEXT_PUBLIC_STAGE}/signatures`,
                    'https://mockend.com/franky358/mockend-api/units',
                     /*{
                         headers: {
                             'x-api-key': process.env.NEXT_PUBLIC_API_KEY,
                             Authorization: 'Bearer ' + userJwt,
                         },
                     }*/
                 )


                units = units.map((unit) => ({
                    ...unit,
                    value: unit.name,
                    label: unit.name,
                }))    
                 setUnits(units);
      
                 const signatureUnits = units.filter((unit) => unit.signatureId === signatures[0].id)
                 setUnitsSelect(signatureUnits);
                 setSelectedUnit(signatureUnits[0])


                let { data: topics } = await axios.get(
                    // `${process.env.NEXT_PUBLIC_AMAZON_URL}/${process.env.NEXT_PUBLIC_STAGE}/signatures`,
                    'https://mockend.com/franky358/mockend-api/topics',
                     /*{
                         headers: {
                             'x-api-key': process.env.NEXT_PUBLIC_API_KEY,
                             Authorization: 'Bearer ' + userJwt,
                         },
                     }*/
                 )
                 topics = topics.map((topic) => ({
                    ...topic,
                    value: topic.name,
                    label: topic.name,
                }))    
                setTopics(topics);

                const unitTopics = topics.filter((topic) => topic.unitId === signatureUnits[0].id)
                setTopicsSelect(unitTopics);
                setSelectedTopic(unitTopics[0])

                 


                let { data: subtopics } = await axios.get(
                    // `${process.env.NEXT_PUBLIC_AMAZON_URL}/${process.env.NEXT_PUBLIC_STAGE}/signatures`,
                    'https://mockend.com/franky358/mockend-api/subtopics',
                     /*{
                         headers: {
                             'x-api-key': process.env.NEXT_PUBLIC_API_KEY,
                             Authorization: 'Bearer ' + userJwt,
                         },
                     }*/
                 )

                 subtopics = subtopics.map((subtopic) => ({
                    ...subtopic,
                    value: subtopic.name,
                    label: subtopic.name,
                }))    
                setSubtopics(subtopics);
                const topicSubtopics = subtopics.filter((subtopic) => subtopic.topicsId === unitTopics[0].id)
                setSubtopicsSelect(topicSubtopics);
                setSelectedSubtopic(topicSubtopics[0])

               setIsLoading(false);

      
                    
            } catch (e) {
                setIsLoading(false);
                console.log(e)
            }
        }
        if (userJwt) getData()
    }, [userJwt])

    /*const selectTextFile = (e) => {
        uploadTextFile(e.target.files[0])
    }

    const selectSubtextFile = (e) => {
        uploadSubtextFile(e.target.files[0])
    }

    const selectOptionOneFile = (e) => {
        uploadOptionOneFile(e.target.files[0])
    }

    const selectOptionTwoFile = (e) => {
        uploadOptionTwoFile(e.target.files[0])
    }

    const selectOptionThreeFile = (e) => {
        uploadOptionThreeFile(e.target.files[0])
    }

    const selectOptionFourFile = (e) => {
        uploadOptionFourFile(e.target.files[0])
    }

    const selectVideoQuestionFile = (e) => {
        uploadVideoQuestion(e.target.files[0])
    }

    const selectImageQuestionFile = (e) => {
        uploadImageQuestion(e.target.files[0])
    }

    const isValidImage = (size, format) => {
        if(size > 2000000 || format !== 'image/png') return false
        return true;
    }

    const isValidVideo = (size, format) => {
        if(size > 10000000 || format !== 'video/mp4') return false
        return true;
    }

    const uploadTextFile = async (fileText) => {
        if(!isValidImage(fileText.size, fileText.type)){
            toast.error(
                `Hubo un problema cargando la imagen. Favor de asegurarte que pese máximo 2MB y sea formato .png 🤔`
            )
            setTextUploadedFile('')
        }
        else{
            setIsLoading(true)
            try {
                let { data } = await axios.post('/api/s3/uploadFile', {
                    name: fileText.name,
                    type: fileText.type,
                })
                const url = data.url
                await axios.put(url, fileText, {
                    headers: {
                        'Content-type': fileText.type,
                        'Access-Control-Allow-Origin': '*',
                    },
                })
                toast.success(`Imagen cargada correctamente 🙂`)
    
                setTextUploadedFile(
                    (BUCKET_URL + fileText.name).replace(/\s+/g, '')
                )
                setIsLoading(false)
            } catch (e) {
                toast.error(
                    `Hubo un problema cargando la imagen. Favor de intentar más tarde 🤔`
                )
                setIsLoading(false)
                console.log(e)
            }
        }

    }

    const uploadSubtextFile = async (fileSubtext) => {
        if(!isValidImage(fileSubtext.size, fileSubtext.type)){
            toast.error(
                `Hubo un problema cargando la imagen. Favor de asegurarte que pese máximo 2MB y sea formato .png 🤔`
            )
            setSubtextUploadedFile('')
        } else {

            try {
                setIsLoading(true)
                let { data } = await axios.post('/api/s3/uploadFile', {
                    name: fileSubtext.name,
                    type: fileSubtext.type,
                })
    
                const url = data.url
                let { data: newData } = await axios.put(url, fileSubtext, {
                    headers: {
                        'Content-type': fileSubtext.type,
                        'Access-Control-Allow-Origin': '*',
                    },
                })
                toast.success(`Imagen cargada correctamente 🙂`)
                setSubtextUploadedFile(
                    (BUCKET_URL + fileSubtext.name).replace(/\s+/g, '')
                )
                setIsLoading(false)
            } catch (e) {
                console.log(e)
                toast.error(
                    `Hubo un problema cargando la imagen. Favor de intentar más tarde 🤔`
                )
                setIsLoading(false)
            }
        }
    }

    const uploadOptionOneFile = async (fileText) => {
        if(!isValidImage(fileText.size, fileText.type)){
            toast.error(
                `Hubo un problema cargando la imagen. Favor de asegurarte que pese máximo 2MB y sea formato .png 🤔`
            )
            setOptionOneUploadedFile('')
        } else {
            try {
                setIsLoading(true)
                let { data } = await axios.post('/api/s3/uploadFile', {
                    name: fileText.name,
                    type: fileText.type,
                })
                const url = data.url
                await axios.put(url, fileText, {
                    headers: {
                        'Content-type': fileText.type,
                        'Access-Control-Allow-Origin': '*',
                    },
                })
                toast.success(`Imagen cargada correctamente 🙂`)
    
                setOptionOneUploadedFile(
                    (BUCKET_URL + fileText.name).replace(/\s+/g, '')
                )
             
                setOption1({
                    ...option1,
                    value: (BUCKET_URL + fileText.name).replace(/\s+/g, ''),
                })
                setIsLoading(false)
            } catch (e) {
                toast.error(
                    `Hubo un problema cargando la imagen. Favor de intentar más tarde 🤔`
                )
            
                setIsLoading(false)
                console.log(e)
            }
        }
    }

    const uploadOptionTwoFile = async (fileText) => {
        if(!isValidImage(fileText.size, fileText.type)){
            toast.error(
                `Hubo un problema cargando la imagen. Favor de asegurarte que pese máximo 2MB y sea formato .png 🤔`
            )
            setOptionTwoUploadedFile('')
        } else {

            try {
                setIsLoading(true)
                let { data } = await axios.post('/api/s3/uploadFile', {
                    name: fileText.name,
                    type: fileText.type,
                })
                const url = data.url
                await axios.put(url, fileText, {
                    headers: {
                        'Content-type': fileText.type,
                        'Access-Control-Allow-Origin': '*',
                    },
                })
                toast.success(`Imagen cargada correctamente 🙂`)
    
                setOptionTwoUploadedFile(
                    (BUCKET_URL + fileText.name).replace(/\s+/g, '')
                )
             
                setOption2({
                    ...option2,
                    value: (BUCKET_URL + fileText.name).replace(/\s+/g, ''),
                })
                setIsLoading(false)
            } catch (e) {
                toast.error(
                    `Hubo un problema cargando la imagen. Favor de intentar más tarde 🤔`
                )
                setIsLoading(false)
                console.log(e)
            }
        }

    }

    const uploadOptionThreeFile = async (fileText) => {

        if(!isValidImage(fileText.size, fileText.type)){
            toast.error(
                `Hubo un problema cargando la imagen. Favor de asegurarte que pese máximo 2MB y sea formato .png 🤔`
            )
            setOptionThreeUploadedFile('')
        } else{
            setIsLoading(true)
            try {
                let { data } = await axios.post('/api/s3/uploadFile', {
                    name: fileText.name,
                    type: fileText.type,
                })
                const url = data.url
                await axios.put(url, fileText, {
                    headers: {
                        'Content-type': fileText.type,
                        'Access-Control-Allow-Origin': '*',
                    },
                })
                toast.success(`Imagen cargada correctamente 🙂`)
    
                setOptionThreeUploadedFile(
                    (BUCKET_URL + fileText.name).replace(/\s+/g, '')
                )
               
                setOption3({
                    ...option3,
                    value: (BUCKET_URL + fileText.name).replace(/\s+/g, ''),
                })
                setIsLoading(false)
            } catch (e) {
                toast.error(
                    `Hubo un problema cargando la imagen. Favor de intentar más tarde 🤔`
                )
                setIsLoading(false)
                console.log(e)
            }
        }
        
        
    }

    const uploadOptionFourFile = async (fileText) => {

        if(!isValidImage(fileText.size, fileText.type)){
            toast.error(
                `Hubo un problema cargando la imagen. Favor de asegurarte que pese máximo 2MB y sea formato .png 🤔`
            )
            setOptionFourUploadedFile('')
        } else {
            try {
                setIsLoading(true)
                let { data } = await axios.post('/api/s3/uploadFile', {
                    name: fileText.name,
                    type: fileText.type,
                })
                const url = data.url
                await axios.put(url, fileText, {
                    headers: {
                        'Content-type': fileText.type,
                        'Access-Control-Allow-Origin': '*',
                    },
                })
                toast.success(`Imagen cargada correctamente 🙂`)
    
                setOptionFourUploadedFile(
                    (BUCKET_URL + fileText.name).replace(/\s+/g, '')
                )
                setOption4({
                    ...option4,
                    value: (BUCKET_URL + fileText.name).replace(/\s+/g, ''),
                })
                setIsLoading(false)
            } catch (e) {
                toast.error(
                    `Hubo un problema cargando la imagen. Favor de intentar más tarde 🤔`
                )
                setIsLoading(false)
                console.log(e)
            }
        }      

    }

    const uploadVideoQuestion = async (fileText) => {
        if(!isValidVideo(fileText.size, fileText.type)){
            toast.error(
                `Hubo un problema cargando el video. Favor de asegurarte que pese máximo 10MB y sea formato .mp4 🤔`
            )
            setVideoQuestionUploadedFile('')
        } else {
            try {
                setIsLoading(true)
                let { data } = await axios.post('/api/s3/uploadFile', {
                    name: fileText.name,
                    type: fileText.type,
                })
                const url = data.url
                await axios.put(url, fileText, {
                    headers: {
                        'Content-type': fileText.type,
                        'Access-Control-Allow-Origin': '*',
                    },
                })
                toast.success(`Video cargado correctamente 🙂`)
    
                setVideoQuestionUploadedFile(
                    (BUCKET_URL + fileText.name).replace(/\s+/g, '')
                )
                setIsLoading(false)
            } catch (e) {
                toast.error(
                    `Hubo un problema cargando el video. Favor de intentar más tarde 🤔`
                )
                setIsLoading(false)
                console.log(e)
            }
        }

    }

    const uploadImageQuestion = async (fileText) => {

        if(!isValidImage(fileText.size, fileText.type)){
            toast.error(
                `Hubo un problema cargando la imagen. Favor de asegurarte que pese máximo 2MB y sea formato .png 🤔`
            )
            setTextUploadedFile('')
        } else {
            try {
                setIsLoading(true)
                let { data } = await axios.post('/api/s3/uploadFile', {
                    name: fileText.name,
                    type: fileText.type,
                })
                const url = data.url
                await axios.put(url, fileText, {
                    headers: {
                        'Content-type': fileText.type,
                        'Access-Control-Allow-Origin': '*',
                    },
                })
                toast.success(`Imagen cargada correctamente 🙂`)
    
                setImageQuestionUploadedFile(
                    (BUCKET_URL + fileText.name).replace(/\s+/g, '')
                )
                setIsLoading(false)
            } catch (e) {
                toast.error(
                    `Hubo un problema cargando la imagen. Favor de intentar más tarde 🤔`
                )
                setIsLoading(false)
                console.log(e)
            }
        }
        
    }
    
    
    


    const validateOptionsColumn = () => {

        let error = false;
        const columnAIds = [1, 2, 3, 4, 5]
        const columnBIds = ['A', 'B', 'C', 'D', 'E']

        const idsColumnAErrors = [];
        const idsColumnBErrors = [];

        if(columnAOptions.length === 0 || columnBOptions.length === 0){
            if(columnAOptions.length === 0) {
                for(let i = 0; i < totalOptionsColumnA; i++){
                    idsColumnAErrors.push(columnAIds[i]);
                }
                setErrorsIdsColumnA(idsColumnAErrors)             
            }
            if(columnBOptions.length === 0) {

                for(let i = 0; i < totalOptionsColumnB; i++){
                    idsColumnBErrors.push(columnBIds[i]);
                }
                setErrorsIdsColumnB(idsColumnBErrors)
            }
            return true;
        }
   
        columnAOptions.forEach(option => {
            if(!option.option){
                idsColumnAErrors.push(option.id);
                error = true;
            }
        });
        setErrorsIdsColumnA(idsColumnAErrors)

        columnBOptions.forEach(option => {
            if(!option.option){
                idsColumnBErrors.push(option.id);
                error = true;
            }
        });
        setErrorsIdsColumnB(idsColumnBErrors)
        return error;
    }

    const validateColumnsTitles = () => {
        let error = false;
        if(!titleColumnA){
            setErrorTitleColumnA('Ingresa el titulo de esta columna')
            error = true;
        } 
        if(!titleColumnB){
            setErrorTitleColumnB('Ingresa el titulo de esta columna')
            error = true;
        }
        return error;
    }

    const hasErrorsExercerciseForm = () => {

        let error = false;
        if (!textExcercise) {
            setTextExcerciseError(
                'Tienes que agregar un texto  para el pregunta '
            )
            error = true
        }
        if(!optionOneExcercise.value){
            setOptionOneExcerciseError('Tienes que agregar la opción 1')
            error = true;
        }
        if(!optionTwoExcercise.value){
            setOptionTwoExcerciseError('Tienes que agregar la opción 2')
            error = true;
        }
        if(!optionThreeExcercise.value){
            setOptionThreeExcerciseError('Tienes que agregar la opción 3')
            error = true;
        }
        if(!optionFourExcercise.value){
            setOptionFourExcerciseError('Tienes que agregar la opción 4')
            error = true;
        }
        return error;
    }

    const hasErrorsQuestionForm = () => {
        let error = false
        if (!textQuestion && !textUploadedFile) {
            setTextQuestionError(
                'Tienes que agregar un texto o una imagen para la pregunta '
            )
            error = true
        }
        if (!option1.value) {
            setOptionOneError('Tienes que agregar la opción 1')
            error = true
        }
        if (!option2.value) {
            setOptionTwoError('Tienes que agregar la opción 2')
            error = true
        }
        if (!option3.value) {
            setOptionThreeError('Tienes que agregar la opción 3')
            error = true
        }
        if (!option4.value) {
            setOptionFourError('Tienes que agregar la opción 4')
            error = true
        }

        if(selectedKindQuestion.value === 'columnsRelation'){        
            let errorOptions = validateOptionsColumn();
            if(errorOptions){
                error = true;
            }
            let errorTitles = validateColumnsTitles();
            if(errorTitles){
                error = true;
            }          
        }

        if(hasErrorsExercerciseForm())
        {
            error = true;
        }
        return error
    }



    const handleSubmitQuestion = async () => {
      
        const dataQuestion = {
            idSignature: selectedSignature.id,
            unit: selectedUnit.value,
            topic: selectedTopic.value,
            subtopic: selectedSubtopic.value,
            type: selectedKindQuestion.value,
            difficulty: selectedDifficulty.value,
            text: textQuestion ? textQuestion : textUploadedFile,
            subtext: subtextQuestion ? subtextQuestion : subtextUploadedFile,
            options: [
                {
                    id: uuidv4(),
                    label: option1.value,
                    value: option1.value,
                    selected: false,
                    color: 'Bryan pink',
                    letter: `${option1.letter}.`,
                    heightImage: null,
                    optionNumber: option1.option,
                },
                {
                    id: uuidv4(),
                    label: option2.value,
                    value: option2.value,
                    selected: false,
                    color: 'Bryan pink',
                    letter: `${option2.letter}.`,
                    heightImage: null,
                    optionNumber: option2.option,
                },
                {
                    id: uuidv4(),
                    label: option3.value,
                    value: option3.value,
                    selected: false,
                    color: 'Bryan pink',
                    letter: `${option3.letter}.`,
                    heightImage: null,
                    optionNumber: option3.option,
                },
                {
                    id: uuidv4(),
                    label: option4.value,
                    value: option4.value,
                    selected: false,
                    color: 'Bryan pink',
                    letter: `${option4.letter}.`,
                    heightImage: null,
                    optionNumber: option4.option,
                },
            ],
            correctAnswer: Number(selectedCorrectAnswer.value),
            columns: selectedKindQuestion.value === 'columnsRelation' ? {
                "columnA": {
                    "title": titleColumnA,
                    "options": columnAOptions
                }, 
                "columnB": {
                    "title": titleColumnB,
                    "options": columnBOptions
                }, 

            } : undefined
        }

        if (!hasErrorsQuestionForm()) { 
            //console.log(dataQuestion)
            setIsLoading(true)
            try {
                const question = await axios.post(
                    `${process.env.NEXT_PUBLIC_AMAZON_URL}/${process.env.NEXT_PUBLIC_STAGE}/questions`,
                    dataQuestion,
                    {
                        headers: {
                            'Content-Type': 'application/json',
                            'x-api-key': process.env.NEXT_PUBLIC_API_KEY,
                            Authorization: 'Bearer ' + userJwt,
                        },
                    }
                )
                toast.success(`Pregunta guardada correctamente 🥳`)
                setTextQuestion('')
                setSubtextQuestion('')
                setHasImageText(false)
                setTextUploadedFile('')
                setHasImageSubtext(false)
                setSubtextUploadedFile('')
                setOption1({ ...option1, value: '' })
                setOption2({ ...option2, value: '' })
                setOption3({ ...option3, value: '' })
                setOption4({ ...option4, value: '' })
                setSelectedCorrectAnswer(correctAnswers[0])
                setSelectedDifficulty(difficulty[0])

                setIsLoading(false)
                setCleanColumnsForm(true);
                //console.log(question)
            } catch (e) {
                toast.error(
                    `Hubo un error al guardar la pregunta. Intenta más tarde por favor🤔`
                    )
                setIsLoading(false)

            }
        }  else{
            toast.error(
                `Favor de revisar los campos faltantes🤔`
                )
        }
    }

    const handleColumnsOptions = (column, columnOptions) => {
        if(column === 'A'){
            setColumnAOptions(columnOptions)
            
        } else {
            setColumnBOptions(columnOptions)
        }

    }

    const handleColumnsTitles = (column, title) => {
        if(column === 'A'){
            setTitleColumnA(title)
            setErrorTitleColumnA('')
        } else {
            setTitleColumnB(title)
            setErrorTitleColumnB('')

        }
    }

    const handleTotalOptionsColumns = (column, total) => {
        if(column === 'A'){
            setTotalOptionsColumnA(total);
        } else {
            setTotalOptionsColumnB(total);
            
        }
    }

    const handleRemoveIdFromError = (column, id) => {
        if(column === 'A'){
            setErrorsIdsColumnA(errorsIdsColumnA.filter(idColumn =>  idColumn !== id));
        } else {
            setErrorsIdsColumnB(errorsIdsColumnB.filter(idColumn =>  idColumn !== id));

        }
    }*/

   // console.log(signatures);

    const handleChangeSignature = (signature) => {
        const signatureUnits = units.filter((unit) => unit.signatureId === signature.id)
        setSelectedSignature(signature);
        setUnitsSelect(signatureUnits);
        setSelectedUnit(signatureUnits[0])
        const unitTopics = topics.filter((topic) => topic.unitId === signatureUnits[0].id)
        setTopicsSelect(unitTopics);
        setSelectedTopic(unitTopics[0])
        const topicSubtopics = subtopics.filter((subtopic) => subtopic.topicsId === unitTopics[0].id)
        //console.log(topicSubtopics)
        setSubtopicsSelect(topicSubtopics);
        setSelectedSubtopic(topicSubtopics[0])
    }

    const handleChangeUnit = (unit) => {
      //  console.log(unit);
      const unitTopics = topics.filter((topic) => topic.unitId === unit.id)
      setSelectedUnit(unit);
      if(unitTopics.length > 0){
        const topicSubtopics = subtopics.filter((subtopic) => subtopic.topicsId === unitTopics[0].id)
        setTopicsSelect(unitTopics)
        setSelectedTopic(unitTopics[0])
        setSubtopicsSelect(topicSubtopics);
        setSelectedSubtopic(topicSubtopics[0])
      } else {
        setTopicsSelect([])
        setSelectedTopic({})
        setSubtopicsSelect([])
        setSelectedSubtopic({})
      }
    }

    const handleChangeTopic = (topic) => {
        setSelectedTopic(topic);
        const topicSubtopics = subtopics.filter((subtopic) => subtopic.topicsId === topic.id)
        if(topicSubtopics.length > 0){
            setSubtopicsSelect(topicSubtopics);
            setSelectedSubtopic(topicSubtopics[0]);
        } else {
            setSubtopicsSelect([]);
            setSelectedSubtopic({})
        }
       

    }
   
    

    return (
        <>
            {isLoading && (
                <ClipLoader
                    color={'#ec4899'}
                    size={40}
                    aria-label="Loading Spinner"
                    data-testid="loader"
                    loading={isLoading}
                    cssOverride={override}
                />
            )}

            {signatures.length > 0  && unitsSelect.length > 0 && (
                <div className="container rounded-lg  text-[#F900BF] bg-gray-100  h-auto  mt-8 py-6 mb-8 px-16 max-w-4xl ">
                    <ToastContainer />

                        <>
                            <h1 className="mt-4">Agrega tus preguntas</h1>
                            <div className="flex mt-8 justify-start ">
                                <div>
                                    <span className="w-54">
                                        Elige una materia
                                    </span>
                                    <Select
                                        defaultValue={signatures[0]}
                                        onChange={((e) => {
                                            handleChangeSignature(e);

                                        })}
                                        options={signatures}
                                        isDisabled={isLoading}
                                    />
                                </div>
                                <div className="ml-12">
                                    <span className="w-54">
                                        Elige el tipo de pregunta
                                    </span>
                                    <Select
                                        defaultValue={kindQuestion[0]}
                                        onChange={setSelectedKindQuestion}
                                        options={kindQuestion}
                                        isDisabled={isLoading}
                                    />
                                </div>
                            </div>

                            <div className="flex justify-between mt-8">
                                <div className="flex flex-col">
                                    <span>Unidad</span>
                                    <Select
                                           onChange={((e) => {
                                               handleChangeUnit(e);
                                           })}
                                           options={unitsSelect}
                                           isDisabled={isLoading}
                                           className='w-40'
                                           value={selectedUnit}
                                    />
                                </div>
                               <div className="flex flex-col">
                                    <span>Tema</span>
                                    <Select
                                        onChange={((e) => {
                                            handleChangeTopic(e);
                                        })}
                                        options={topicsSelect}
                                        isDisabled={isLoading}
                                        value={selectedTopic}
                                        className='w-40'


                                    />
                                </div>
                                <div className="flex flex-col">
                                    <span>Subtemas</span>
                                    <Select
                                        onChange={setSelectedSubtopic}                                       
                                        options={subtopicsSelect}
                                        isDisabled={isLoading}
                                        value={selectedSubtopic}
                                        className='w-40'

                                    />
                                    </div>
                            </div>
                            {/*<div className="mt-8">
                                <div className="flex">
                                    <span>Texto Pregunta</span>
                                    <div className="ml-8 flex items-center">
                                        <Toggle
                                            id="text-image-status"
                                            onChange={() => {
                                                setHasImageText(!hasImageText)
                                            }}
                                            disabled={isLoading}
                                            checked={hasImageText}
                                        />
                                        <label
                                            className="ml-2"
                                            htmlFor="text-image-status"
                                        >
                                            Es imagen?
                                        </label>
                                    </div>
                                </div>
                                <div className="mt-4">
                                    {hasImageText ? (
                                        <>
                                            <div>Subir imagen</div>

                                            <input
                                                className="mt-2"
                                                type="file"
                                                onChange={(e) =>
                                                    selectTextFile(e)
                                                }
                                                disabled={isLoading}
                                            />
                                     
                                            {textUploadedFile && (
                                                <img
                                                    className="mt-4"
                                                    src={textUploadedFile}
                                                />
                                            )}
                                        </>
                                    ) : (
                                        <textarea
                                            className="mt-2 p-3 rounded"
                                            cols={70}
                                            rows={4}
                                            value={textQuestion}
                                            disabled={isLoading}
                                            onChange={(e) => {
                                                setTextQuestion(e.target.value)
                                                setTextQuestionError('')
                                            }}
                                        />
                                    )}
                                        </div>

                                <div className="mt-2">
                                    {textQuestionError && (
                                        <p className="text-rose-600">
                                            {textQuestionError}
                                        </p>
                                    )}
                                </div>
                            </div>*/}

                            {/*<div className="mt-8">
                                <div className="flex">
                                    <span>Subtexto Pregunta</span>
                                    <div className="ml-8 flex items-center">
                                        <Toggle
                                            id="text-image-status"
                                            onChange={() => {
                                                setHasImageSubtext(
                                                    !hasImageSubtext
                                                )
                                            }}
                                            disabled={isLoading}
                                            checked={hasImageSubtext}
                                        />
                                        <label
                                            className="ml-2"
                                            htmlFor="text-image-status"
                                        >
                                            Es imagen?
                                        </label>
                                    </div>
                                </div>
                                <div className="mt-4">
                                    {hasImageSubtext ? (
                                        <>
                                            <div>Subir imagen</div>

                                            <input
                                                className="mt-2"
                                                type="file"
                                                onChange={(e) =>
                                                    selectSubtextFile(e)
                                                }
                                                disabled={isLoading}

                                            />
                                          
                                            {subtextUploadedFile && (
                                                <img
                                                    className="mt-4"
                                                    src={subtextUploadedFile}
                                                />
                                            )}
                                        </>
                                    ) : (
                                        <textarea
                                            className="mt-2 p-3 rounded"
                                            cols={70}
                                            rows={4}
                                            value={subtextQuestion}
                                            disabled={isLoading}
                                            onChange={(e) =>
                                                setSubtextQuestion(
                                                    e.target.value
                                                )
                                            }
                                        />
                                    )}
                                </div>
                            </div>*/}

                            {/*selectedKindQuestion.value ===
                                'columnsRelation' && (
                                <div className="mt-8">
                                    <p className="text-[#9900F0] font-bold text-center">
                                        Información de las columnas
                                    </p>
                

                                    <div className="flex justify-between">
                                        <Column  column="A" handleColumnsOptions={handleColumnsOptions} handleColumnsTitles={handleColumnsTitles} errorTitleColumnA={errorTitleColumnA} errorsIdsColumnA={errorsIdsColumnA}  handleTotalOptionsColumns={handleTotalOptionsColumns} handleRemoveIdFromError={handleRemoveIdFromError} cleanColumnsForm={cleanColumnsForm}/>
                                        <Column  column="B" handleColumnsOptions={handleColumnsOptions} handleColumnsTitles={handleColumnsTitles} errorTitleColumnB={errorTitleColumnB} errorsIdsColumnB={errorsIdsColumnB} handleTotalOptionsColumns={handleTotalOptionsColumns} handleRemoveIdFromError={handleRemoveIdFromError} cleanColumnsForm={cleanColumnsForm} />
                                    </div>
                                </div>
                                )*/}

                           {/* <div className="mt-8">
                                <p className="text-[#9900F0] font-bold text-center">
                                    Respuestas
                                </p>
                                <div className="mt-8">
                                    <div className="flex">
                                        <span className="mt-2">
                                            Opción: {option1.option}
                                        </span>
                                        <span className="mt-2 ml-8">
                                            Letra: {option1.letter}
                                        </span>
                                        <div className="ml-8 flex items-center">
                                            <Toggle
                                                id="text-image-status"
                                                onChange={() => {
                                                    setHasImageOptionOne(
                                                        !hasImageOptionOne
                                                    )
                                                }}
                                                disabled={isLoading}
                                                checked={hasImageOptionOne}
                                            />
                                            <label
                                                className="ml-2"
                                                htmlFor="text-image-status"
                                            >
                                                Es imagen?
                                            </label>
                                        </div>
                                    </div>

                                    <div className="mt-4">
                                        {hasImageOptionOne ? (
                                            <>
                                                <div>Subir imagen</div>

                                                <input
                                                    className="mt-2"
                                                    type="file"
                                                    onChange={(e) =>
                                                        selectOptionOneFile(e)
                                                    }
                                                    disabled={isLoading}
                                                />
                                           
                                         
                                                {optionOneUploadedFile && (
                                                    <img
                                                        className="mt-4"
                                                        src={
                                                            optionOneUploadedFile
                                                        }
                                                    />
                                                )}
                                            </>
                                        ) : (
                                            <textarea
                                                className="mt-2 p-3 rounded"
                                                cols={40}
                                                rows={2}
                                                value={option1.value}
                                                disabled={isLoading}
                                                onChange={(e) => {
                                                    setOption1({
                                                        ...option1,
                                                        value: e.target.value,
                                                    })
                                                    setOptionOneError('')
                                                }}
                                            />
                                        )}
                                    </div>

                                    <div className="mt-2">
                                        {optionOneError && (
                                            <p className="text-rose-600">
                                                {optionOneError}
                                            </p>
                                        )}
                                    </div>
                            </div>
                                <div className="mt-4">
                                    <div className="flex">
                                        <span className="mt-2">
                                            Opción: {option2.option}
                                        </span>
                                        <span className="mt-2 ml-8">
                                            Letra: {option2.letter}
                                        </span>
                                        <div className="ml-8 flex items-center">
                                            <Toggle
                                                id="text-image-status"
                                                onChange={() => {
                                                    setHasImageOptionTwo(
                                                        !hasImageOptionTwo
                                                    )
                                                }}
                                                disabled={isLoading}
                                                checked={hasImageOptionTwo}
                                            />
                                            <label
                                                className="ml-2"
                                                htmlFor="text-image-status"
                                            >
                                                Es imagen?
                                            </label>
                                        </div>
                                    </div>
                                    <div className="mt-4">
                                        {hasImageOptionTwo ? (
                                            <>
                                                <div>Subir imagen</div>

                                                <input
                                                    className="mt-2"
                                                    type="file"
                                                    onChange={(e) =>
                                                        selectOptionTwoFile(e)
                                                    }
                                                    disabled={isLoading}
                                                />
                                         
                                                {optionTwoUploadedFile && (
                                                    <img
                                                        className="mt-4"
                                                        src={
                                                            optionTwoUploadedFile
                                                        }
                                                    />
                                                )}
                                            </>
                                        ) : (
                                            <textarea
                                                className="mt-2 p-3 rounded"
                                                cols={40}
                                                rows={2}
                                                value={option2.value}
                                                disabled={isLoading}
                                                onChange={(e) => {
                                                    setOption2({
                                                        ...option2,
                                                        value: e.target.value,
                                                    })
                                                    setOptionTwoError('')
                                                }}
                                            />
                                        )}
                                    </div>
                                    <div className="mt-2">
                                        {optionTwoError && (
                                            <p className="text-rose-600">
                                                {optionTwoError}
                                            </p>
                                        )}
                                    </div>
                                </div>
                            <div className="mt-4">
                                    <div className="flex">
                                        <span className="mt-2">
                                            Opción: {option3.option}
                                        </span>
                                        <span className="mt-2 ml-8">
                                            Letra: {option3.letter}
                                        </span>
                                        <div className="ml-8 flex items-center">
                                            <Toggle
                                                id="text-image-status"
                                                onChange={() => {
                                                    setHasImageOptionThree(
                                                        !hasImageOptionThree
                                                    )
                                                }}
                                                disabled={isLoading}
                                                checked={hasImageOptionThree}
                                            />
                                            <label
                                                className="ml-2"
                                                htmlFor="text-image-status"
                                            >
                                                Es imagen?
                                            </label>
                                        </div>
                                    </div>
                                    <div className="mt-4">
                                        {hasImageOptionThree ? (
                                            <>
                                                <div>Subir imagen</div>

                                                <input
                                                    className="mt-2"
                                                    type="file"
                                                    onChange={(e) =>
                                                        selectOptionThreeFile(e)
                                                    }
                                                    disabled={isLoading}
                                                />
        
                                                {optionThreeUploadedFile && (
                                                    <img
                                                        className="mt-4"
                                                        src={
                                                            optionThreeUploadedFile
                                                        }
                                                    />
                                                )}
                                            </>
                                        ) : (
                                            <textarea
                                                className="mt-2 p-3 rounded"
                                                cols={40}
                                                rows={2}
                                                value={option3.value}
                                                disabled={isLoading}
                                                onChange={(e) => {
                                                    setOption3({
                                                        ...option3,
                                                        value: e.target.value,
                                                    })
                                                    setOptionThreeError('')
                                                }}
                                            />
                                        )}
                                    </div>
                                    <div className="mt-2">
                                        {optionThreeError && (
                                            <p className="text-rose-600">
                                                {optionThreeError}
                                            </p>
                                        )}
                                    </div>
                            </div>
                                <div className="mt-4">
                                    <div className="flex">
                                        <span className="mt-2">
                                            Opción: {option4.option}
                                        </span>
                                        <span className="mt-2 ml-8">
                                            Letra: {option4.letter}
                                        </span>
                                        <div className="ml-8 flex items-center">
                                            <Toggle
                                                id="text-image-status"
                                                onChange={() => {
                                                    setHasImageOptionFour(
                                                        !hasImageOptionFour
                                                    )
                                                }}
                                                disabled={isLoading}
                                                checked={hasImageOptionFour}
                                            />
                                            <label
                                                className="ml-2"
                                                htmlFor="text-image-status"
                                            >
                                                Es imagen?
                                            </label>
                                        </div>
                                    </div>
                                    <div className="mt-4">
                                        {hasImageOptionFour ? (
                                            <>
                                                <div>Subir imagen</div>

                                                <input
                                                    className="mt-2"
                                                    type="file"
                                                    onChange={(e) =>
                                                        selectOptionFourFile(e)
                                                    }
                                                    disabled={isLoading}
                                                />
                                           
                                     
                                                {optionFourUploadedFile && (
                                                    <img
                                                        className="mt-4"
                                                        src={
                                                            optionFourUploadedFile
                                                        }
                                                    />
                                                )}
                                            </>
                                        ) : (
                                            <textarea
                                                className="mt-2 p-3 rounded"
                                                cols={40}
                                                rows={2}
                                                value={option4.value}
                                                disabled={isLoading}
                                                onChange={(e) => {
                                                    setOption4({
                                                        ...option4,
                                                        value: e.target.value,
                                                    })
                                                    setOptionFourError('')
                                                }}
                                            />
                                        )}
                                    </div>
                                    <div className="mt-2">
                                        {optionFourError && (
                                            <p className="text-rose-600">
                                                {optionFourError}
                                            </p>
                                        )}

                                    </div>
                                </div>
                            </div> */}

                           {/* <div className="mt-4 flex items-center">
                                <div className="flex flex-col">
                                    <span>Respuesta Correcta</span>
                                    <Select
                                        defaultValue={correctAnswers[0]}
                                        onChange={setSelectedCorrectAnswer}
                                        options={correctAnswers}
                                        className="w-36"
                                        isDisabled={isLoading}
                                        value={selectedCorrectAnswer}
                                    />
                                </div>
                                <div className="flex flex-col ml-8">
                                    <span>Dificultad</span>
                                    <Select
                                        defaultValue={difficulty[0]}
                                        onChange={setSelectedDifficulty}
                                        options={difficulty}
                                        className="w-36"
                                        isDisabled={isLoading}
                                        value={selectedDifficulty}
                                    />
                                </div>
                            </div> */}
                       {/*} <div className='mt-8'>
                            <p className="text-[#9900F0] font-bold text-center">
                                    Multimedia
                            </p>

                            <div className='mt-8 flex justify-around'>
                            <div className='flex flex-col w-1/2'>
                                <span className='text-center'>Video</span>          
                                <input
                                    className="mt-4"
                                    type="file"
                                    onChange={(e) =>
                                        selectVideoQuestionFile(e)
                                    }
                                    disabled={isLoading}
                                />
        
                                {videoQuestionUploadedFile && (
                                    <video
                                        src={videoQuestionUploadedFile}
                                        controls={true}
                                        className="mt-4"
                                />                                            
                                )}
                            </div>
                            <div className='flex flex-col w-1/2'>
                                <span className='text-center'>Imagen</span>
                                <input
                                    className="mt-2"
                                    type="file"
                                    onChange={(e) =>
                                            selectImageQuestionFile(e)
                                    }
                                    disabled={isLoading}
                                />
                                {imageQuestionUploadedFile && (
                                    <img
                                        className="mt-4 px-2"
                                        src={
                                            imageQuestionUploadedFile
                                        }
                                    />
                                )}
                            </div>
                            
                        </div>

                    </div> */}

                {/*    <div className='mt-8'>
                        <p className="text-[#9900F0] font-bold text-center">
                            Ejercicio de repaso
                        </p>

                        <div className='mt-8 flex flex-col'>
                          <span>Texto Ejercicio</span>
                          <textarea
                                className="mt-2 p-3 rounded"
                                cols={70}
                                rows={4}
                                value={textExcercise}
                                disabled={isLoading}
                                onChange={(e) => {
                                    setTextExcercise(e.target.value)
                                    setTextExcerciseError('')
                                }}
                             />
                        </div>
                        <div className="mt-2">
                            {textExcerciseError && (
                                <p className="text-rose-600">
                                    {textExcerciseError}
                                </p>
                            )}
                        </div>
                        <div className='mt-8 flex flex-col'>
                          <span>Subtexto Ejercicio</span>
                          <textarea
                                className="mt-2 p-3 rounded"
                                cols={70}
                                rows={4}
                                value={subtextExcercise}
                                disabled={isLoading}
                                onChange={(e) => {
                                    setSubTextExcercise(e.target.value)
                                }}
                             />
                        </div>
                        <div className='mt-8'>
                            <div className='flex'>
                            <span>Opción 1</span>
                            <span className='ml-4'>Letra A</span>
                            </div>
                            <div>
                            <textarea
                                className="mt-2 p-3 rounded"
                                cols={40}
                                rows={2}
                                value={optionOneExcercise.value}
                                disabled={isLoading}
                                onChange={(e) => {
                                    setOptionOneExcercise({
                                                ...optionOneExcercise,
                                                value: e.target.value,
                                            })
                                    setOptionOneExcerciseError('')
                                }}
                            />
                            </div>
                        </div>
                        <div className="mt-2">
                            {optionOneExcerciseError && (
                                <p className="text-rose-600">
                                                {optionOneExcerciseError}
                                </p>
                            )}
                        </div>
                        <div className='mt-8'>
                            <div className='flex'>
                            <span>Opción 2</span>
                            <span className='ml-4'>Letra B</span>
                            </div>
                            <div>
                            <textarea
                                className="mt-2 p-3 rounded"
                                cols={40}
                                rows={2}
                                value={optionTwoExcercise.value}
                                disabled={isLoading}
                                onChange={(e) => {
                                    setOptionTwoExcercise({
                                                ...optionTwoExcercise,
                                                value: e.target.value,
                                            })
                                    setOptionTwoExcerciseError('')
                                }}
                            />
                            </div>
                        </div>
                        <div className="mt-2">
                            {optionTwoExcerciseError && (
                                <p className="text-rose-600">
                                                {optionTwoExcerciseError}
                                </p>
                            )}
                        </div>

                        <div className='mt-8'>
                            <div className='flex'>
                            <span>Opción 3</span>
                            <span className='ml-4'>Letra C</span>
                            </div>
                            <div>
                            <textarea
                                className="mt-2 p-3 rounded"
                                cols={40}
                                rows={2}
                                value={optionThreeExcercise.value}
                                disabled={isLoading}
                                onChange={(e) => {
                                    setOptionThreeExcercise({
                                                ...optionThreeExcercise,
                                                value: e.target.value,
                                            })
                                    setOptionThreeExcerciseError('')
                                }}
                            />
                            </div>
                        </div>
                        <div className="mt-2">
                            {optionThreeExcerciseError && (
                                <p className="text-rose-600">
                                                {optionThreeExcerciseError}
                                </p>
                            )}
                        </div>    

                        <div className='mt-8'>
                            <div className='flex'>
                            <span>Opción 4</span>
                            <span className='ml-4'>Letra D</span>
                            </div>
                            <div>
                            <textarea
                                className="mt-2 p-3 rounded"
                                cols={40}
                                rows={2}
                                value={optionFourExcercise.value}
                                disabled={isLoading}
                                onChange={(e) => {
                                    setOptionFourExcercise({
                                                ...optionFourExcercise,
                                                value: e.target.value,
                                            })
                                    setOptionFourExcerciseError('')
                                }}
                            />
                            </div>
                        </div>  
                        <div className="mt-2">
                            {optionFourExcerciseError && (
                                <p className="text-rose-600">
                                                {optionFourExcerciseError}
                                </p>
                            )}
                        </div> 

                        <div className="flex flex-col mt-8">
                            <span>Respuesta Correcta</span>
                                <Select
                                    defaultValue={correctAnswersExcercise[0]}
                                    onChange={setSelectedCorrectAnswerExcercise}
                                    options={correctAnswers}
                                    className="w-36"
                                    isDisabled={isLoading}
                                    value={selectedCorrectAnswerExcercise}
                                />
                        </div>   

                    </div> */}
                           {/*} <div className="flex justify-center mt-8">
                                <button
                                    type="button"
                                    disabled={isLoading}
                                    onClick={handleSubmitQuestion}
                                    className={
                                        isLoading
                                            ? 'text-white bg-slate-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center mr-2 mb-2'
                                            : `text-white bg-[#9900F0] hover:bg-gradient-to-bl focus:ring-4 focus:outline-none focus:ring-pink-200 dark:focus:ring-pink-800 font-medium rounded-lg text-sm px-5 py-2.5 text-center mr-2 mb-2`
                                    }
                                >
                                    Guardar Pregunta
                                </button>
                                </div>*/}
                        </>
                    
                </div>
            )}
        </>
    )
}

export default AddQuestions
